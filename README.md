## Synopsis

Welcome to our test automation project! It has been created as a graduation project for a group majoring in Software Testing.
Our main goal was to develop automated tests for the key functionalities of Trello using Selenium WebDriver, Page Object Pattern and Python 2.7.

**Note:** Please do have in mind while browsing effects of our work, that this has been made for educational purposes only.

## Requirements

In order to run tests from this project, you will need:

- Python 2.7.13
- Selenium WebDriver for Python
- ChromeDriver
- Python psutils library
- Python unittest library (available by default)
- configuration available in the file settings.py (you'll need to make one on basis of file sample_settings.py located in Utils folder)
- a valid Trello account
- a group created in Trello and the user from above belonging to that group

## Project Documentation

If our project has caught your attention and you are willing to know more about it, please browse our projects documentation files. They are located in the Documentation folder. 
You will find there all the necessary information and guidelines regarding how our automation tests are built and how to run them.

## Test Reports

A vital part of our testing project is a website with reports from tests. Please visit our [second repo](https://bitbucket.org/katarzyna_boguslawska/wsb-toa-view/) that is dedicated to this matter.

## Contact

Feel free to contact the repository owner for any information on the project.