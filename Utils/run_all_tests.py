import logging
import os
from unittest import TextTestRunner, TestLoader, TestResult
import helpers
import settings
from ProcessTools import settings as process_settings


def get_all_tests():

    logging_orchestrator = helpers.TestArtifactsHelpers()
    logging_orchestrator.set_up_logging(settings.LOG_DIR, 'Trello_complete')

    loader = TestLoader()
    test_suite = loader.discover(process_settings.TEST_REPOSITORY)

    with open(os.devnull, 'w') as null_stream:
        runner = TextTestRunner(stream=null_stream)
        runner.resultclass = TestResult
        result = runner.run(test_suite)
        if not result.wasSuccessful():
            logging.error('The overall result of the test suite Trello in-and-out is FAILED')
        else:
            logging.info('The overall result of the test suite Trello in-and-out is PASSED')
        if result.errors:
            logging.error(result.errors)
        if result.failures:
            logging.error(result.failures)

if __name__ == '__main__':
    get_all_tests()
