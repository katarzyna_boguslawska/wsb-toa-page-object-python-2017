from selenium import webdriver
''' BROWSER VARIABLES'''
BROWSER = webdriver.Chrome

'''ENVIRONMENT-SPECIFIC and ORDER VARIABLES '''
### CHANGE IT TO MATCH YOUR EXISTING DIRECTORY FOR LOGS - WON'T WORK OTHERWISE ###
LOG_DIR = '/home/me/my_selenium_logs'

'''URLS VARIABLES'''
HOMEPAGE = 'https://trello.com/'
VALID_LOGIN_URL = 'https://trello.com/login'
INVALID_LOGIN_URL = 'http://www.seleniumhq.org/'
PASSWORD_RESET = 'https://trello.com/forgot'

'''CREDENTIAL VARIABLES'''
### CHANGE THEM TO MATCH YOUR ACTUAL CREDENTIALS - WON'T WORK OTHERWISE ###
VALID_USER = 'trello_user@example.com'
INVALID_USER = 'non_exisitng_user@example.com'
VALID_PASSWORD = 'trello_password'
INVALID_PASSWORD = 'wrong_password'
USER_GROUP = 'test_group'
NEW_TEAM = 'new_crew'
NEW_MEMBER_EMAIL = 'new_team_member@example.com'


'''CONDITIONS VARIABLES'''
INVALID_USERNAME_TEXT = "There isn't an account for this username"
INVALID_PASSWORD_TEXT = 'Invalid password'
RESET_HEADING_TEXT = 'Reset Your Trello Password'
RESET_DETAILS = 'Help is on the way'

'''ACTION VARIABLES'''
GENERAL_TIMEOUT = 2
ACCEPTABLE_TIMEOUT = 5
LOGIN_TIMEOUT = 4


'''CARD MANIPULATION VARIABLES'''
ATTACHMENT_LINK = 'https://unsplash.com/photos/WNYsopmEpIg'
CHECKLIST_TITLE = 'Checklist #1'
ITEM_ENTRY = 'Item #1'