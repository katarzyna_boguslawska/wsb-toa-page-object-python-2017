import logging
from time import gmtime, strftime
import os
import psutil


class TestArtifactsHelpers():

    def __init__(self):
        self.start_time = gmtime()

    @staticmethod
    def set_up_logging(log_dir, testname):

        path_to_log_file = log_dir
        now = strftime("%Y-%m-%dT%H_%M_%S", gmtime())
        if '\\' in path_to_log_file:
            path_to_list = path_to_log_file.split('\\')
        else:
            path_to_list = path_to_log_file.split('/')
        log_directory = os.path.abspath(os.path.join(os.sep, *path_to_list))
        log_file = os.path.join(log_directory, now+'_' + testname + '.log')
        logging.basicConfig(filename=log_file, level=logging.DEBUG)
        logging.info('Trello %s test started @ %s \n' % (testname.lower(), strftime("%Y-%m-%dT%H_%M_%S", gmtime())))

    @staticmethod
    def get_resources_statistics():
            cpu = psutil.cpu_percent(interval=None)
            mem = psutil.virtual_memory()
            currently_used_mem = mem.used / 1000000
            logging.info('Current CPU usage percentage is %s' % str(cpu))
            logging.info('Current memory consumption is %s MB' % str(currently_used_mem))
