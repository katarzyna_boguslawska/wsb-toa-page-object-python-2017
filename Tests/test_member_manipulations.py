import unittest
import logging
from time import gmtime, strftime
from Utils import settings, helpers
from Objects.login_page import LoginPage
import time
import os
from unittest import TextTestRunner, TestResult, TestLoader


class TeamMemberManipulation(unittest.TestCase):

    logging_orchestrator = helpers.TestArtifactsHelpers()
    logging_orchestrator.set_up_logging(settings.LOG_DIR, 'TeamMemberManipulation')

    def setUp(self):
        self.driver = settings.BROWSER()
        logging.info('\nThe current test run has been performed on %s' % self.driver.capabilities['browserName'])
        login_page = LoginPage(self.driver)
        self.logged_dashboard_page = login_page.log_in(settings.VALID_USER, settings.VALID_PASSWORD)
        creation_menu = self.logged_dashboard_page.open_plus_menu()
        popup = creation_menu.open_personal_team_creation_page(settings.NEW_TEAM)
        self.new_team_profile = popup.create_team(settings.NEW_TEAM)
        self.assertTrue(self.new_team_profile.is_team_created(), 'ERROR! Team has not been created')

    def test_adding_new_member(self):
        logging.info('TEST CASE started: adding new team member')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        members_page = self.new_team_profile.add_member(settings.NEW_MEMBER_EMAIL)
        self.assertTrue(members_page.check_if_members_number_increased(1), 'ERROR! The number of members in the group is still 1')
        self.settings_page = self.new_team_profile.go_to_settings()

    def tearDown(self):
        self.settings_page.delete()
        self.logged_dashboard_page.log_out()
        self.driver.quit()
        logging.info('Trello login test finished @ %s \n' % (strftime("%Y-%m-%dT%H_%M_%S", gmtime())))
        time.sleep(5)

if __name__ == '__main__':
    with open(os.devnull, 'w') as null_stream:
        runner = TextTestRunner(stream=null_stream)
        runner.resultclass = TestResult
        suite = TestLoader().loadTestsFromTestCase(TeamMemberManipulation)
        result = runner.run(suite)

        if not result.wasSuccessful():
            logging.error('The overall result of the test suite Trello logins is FAILED')
        else:
            logging.info('The overall result of the test suite Trello logins is PASSED')
        if result.errors:
            logging.error(result.errors)
        if result.failures:
            logging.error(result.failures)
