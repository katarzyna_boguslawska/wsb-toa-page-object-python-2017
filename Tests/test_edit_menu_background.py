import logging
import os
import time
import unittest
from time import gmtime, strftime
from unittest import TextTestRunner, TestResult, TestLoader

from selenium.common.exceptions import NoSuchElementException

from Objects.login_page import LoginPage
from Utils import helpers
from Utils import settings


class EditMenu(unittest.TestCase):
    logging_orchestrator = helpers.TestArtifactsHelpers()
    logging_orchestrator.set_up_logging(settings.LOG_DIR, 'EditingBackground')
    driver = None
    group_board = None

    @classmethod
    def setUpClass(cls):
        cls.driver = settings.BROWSER()
        logging.info('\nThe current test run has been performed on %s' % cls.driver.capabilities['browserName'])
        login_page = LoginPage(cls.driver)
        logged_dashboard = login_page.log_in(settings.VALID_USER, settings.VALID_PASSWORD)
        cls.group_board = logged_dashboard.create_new_group_board(settings.USER_GROUP, 'Testing Edit menu board')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def setUp(self):
        self.board_menu = self.group_board.show_menu()

    def test_change_background_photo(self):
        self.assertFalse(self.group_board.check_if_background_is_photo())
        self.board_menu.change_background_to_random_photo()
        self.assertTrue(self.group_board.check_if_background_is_photo(), 'ERROR, the background was not changed to a photo')

    def test_change_background_color(self):
        self.assertNotEqual(self.group_board.get_background_color(), "rgba(75, 191, 107, 1)")
        self.board_menu.change_background_to_color('pistachio')
        self.assertEqual(self.group_board.get_background_color(), "rgba(75, 191, 107, 1)", 'ERROR, the background was not changed to pistachio')

    def tearDown(self):
        self.board_menu.go_back_to_menu()
        time.sleep(3)

    @classmethod
    def tearDownClass(cls):
        try:
            cls.group_board.delete_board()
            cls.group_board.log_out()
        except NoSuchElementException:
            logging.error('ERROR! Could not archive lists and delete the board')
        finally:
            cls.driver.quit()
            logging.info('Trello login test finished @ %s \n' % (strftime("%Y-%m-%dT%H_%M_%S", gmtime())))

if __name__ == '__main__':
    with open(os.devnull, 'w') as null_stream:
        runner = TextTestRunner(stream=null_stream)
        runner.resultclass = TestResult
        suite = TestLoader().loadTestsFromTestCase(EditMenu)
        result = runner.run(suite)

        if not result.wasSuccessful():
            logging.error('The overall result of the test suite Trello list and card adding is FAILED')
        else:
            logging.info('The overall result of the test suite Trello list and card adding is PASSED')
        if result.errors:
            logging.error(result.errors)
        if result.failures:
            logging.error(result.failures)

