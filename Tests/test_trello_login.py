import logging
import os
import time
import unittest
from time import gmtime, strftime
from unittest import TextTestRunner, TestLoader, TestResult

from Utils import helpers
from Utils import settings
from Objects.login_page import LoginPage


class TrelloLogin(unittest.TestCase):

    logging_orchestrator = helpers.TestArtifactsHelpers()
    logging_orchestrator.set_up_logging(settings.LOG_DIR, 'TrelloLogin')

    #  create the driver and start log
    def setUp(self):
        self.driver = settings.BROWSER()
        logging.info('\nThe current test run has been performed on %s' % self.driver.capabilities['browserName'])
        self.login_page = LoginPage(self.driver)
        helpers.TestArtifactsHelpers.get_resources_statistics()

    # test of logging in with correct credentials as defined in settings.py
    def test_log_in(self):
        logging.info('TEST CASE started: successful log in')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.login_page.complete_login_credentials(settings.VALID_USER, settings.VALID_PASSWORD)
        self.logged_dashboard = self.login_page.click_submit_button()
        self.assertTrue(self.logged_dashboard.is_create_board_element_present(), 'ERROR! The board is not available for selection')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.logged_dashboard.log_out()

    # test of logging in with incorrect password as defined in settings.py
    def test_login_failure_by_password(self):
        logging.info('TEST CASE started: login failed with wrong password')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.login_page.complete_login(settings.VALID_USER)
        self.login_page.complete_password(settings.INVALID_PASSWORD_TEXT)
        self.failure_page = self.login_page.fail_login()
        self.assertTrue(self.failure_page.is_error_communicated(), 'ERROR! No information about the login failure was displayed')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    # test of logging in with incorrect login as defined in settings.py
    def test_login_failure_by_username(self):
        logging.info('TEST CASE started: login failed with non-existent username')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.login_page.complete_login(settings.INVALID_USER)
        self.assertTrue(self.login_page.is_error_communicated(), 'ERROR! No information about the login failure was displayed')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    # test of availability of the password reset option
    def test_reset_password(self):
        logging.info('TEST CASE started: password reset')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.password_reset_page = self.login_page.ask_for_password_reset(settings.VALID_USER)
        self.password_reset_page.complete_credentials_for_password_reset()
        self.assertTrue(self.password_reset_page.is_email_sent(), 'ERROR! No information about the login reset email being sent was displayed')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    # now that all test cases are done, the driver closes
    def tearDown(self):
        self.driver.quit()
        logging.info('Trello login test finished @ %s \n' % (strftime("%Y-%m-%dT%H_%M_%S", gmtime())))
        time.sleep(5)


if __name__ == '__main__':
    with open(os.devnull, 'w') as null_stream:
        runner = TextTestRunner(stream=null_stream)
        runner.resultclass = TestResult
        suite = TestLoader().loadTestsFromTestCase(TrelloLogin)
        result = runner.run(suite)

        if not result.wasSuccessful():
            logging.error('The overall result of the test suite Trello logins is FAILED')
        else:
            logging.info('The overall result of the test suite Trello logins is PASSED')
        if result.errors:
            logging.error(result.errors)
        if result.failures:
            logging.error(result.failures)


