import unittest
import logging
from time import gmtime, strftime
from Utils import settings
from Utils import helpers
from Objects.login_page import LoginPage
import time
import os
from unittest import TextTestRunner, TestResult, TestLoader
from selenium.common.exceptions import NoSuchElementException


class CardManipulations(unittest.TestCase):

    logging_orchestrator = helpers.TestArtifactsHelpers()
    logging_orchestrator.set_up_logging(settings.LOG_DIR, 'CardManipulations')
    driver = None
    group_board = None

    @classmethod
    def setUpClass(cls):
        cls.driver = settings.BROWSER()
        logging.info('\nThe current test run has been performed on %s' % cls.driver.capabilities['browserName'])
        login_page = LoginPage(cls.driver)
        logged_dashboard = login_page.log_in(settings.VALID_USER, settings.VALID_PASSWORD)
        cls.group_board = logged_dashboard.create_new_group_board(settings.USER_GROUP, 'Testing 1')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        cls.group_board.add_list('Test List 1')
        cls.group_board.add_list('Test List 2')
        cls.group_board.add_list('Test List 3')
        cls.group_board.add_list('Test List 4')
        cls.list1 = cls.group_board.find_list_by_title('Test List 1')
        cls.list2 = cls.group_board.find_list_by_title('Test List 2')
        cls.list3 = cls.group_board.find_list_by_title('Test List 3')
        cls.list4 = cls.group_board.find_list_by_title('Test List 4')
        for card_number in range(0, 6):
            text = 'Card ' + str(card_number + 1) + ' on List 1'
            cls.list1.add_card(text)
        for card_number in range(0, 3):
            text = 'Card ' + str(card_number + 1) + ' on List 2'
            cls.list2.add_card(text)
        for card_number in range(0, 5):
            text = 'Card ' + str(card_number + 1) + ' on List 3'
            cls.list3.add_card(text)
        for card_number in range(0, 3):
            text = 'Card ' + str(card_number + 1) + ' on List 4'
            cls.list4.add_card(text)

    def test_add_deadline(self):
        logging.info('TEST CASE started: adding deadline to a card')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.list1.add_due_date_on_card_with_text('Card 1 on List 1', '11/11/2017')
        self.assertTrue(self.list1.check_if_due_date_was_added_to_card_with_text('Card 1 on List 1'), 'ERROR! deadline was not added')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_archive_card(self):
        logging.info('TEST CASE started: archiving a card')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.list1.archive_card_with_text('Card 2 on List 1')
        self.assertTrue(self.list1.check_if_card_with_text_was_archived('Card 2 on List 1'), 'ERROR! the card is still present on the list')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_add_attachment_from_link(self):
        logging.info('TEST CASE started: adding an attachment from link to an item')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.list3.add_attachment_from_link_to_card_with_text('Card 4 on List 3', settings.ATTACHMENT_LINK)
        self.assertTrue(self.list3.check_if_attachment_was_added_to_the_card_with_text('Card 4 on List 3'), 'ERROR! the attachment is absent')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_serialize_to_json(self):
        logging.info('TEST CASE started: exporting a card to json')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        json_exported_page = self.list2.export_to_json_card_with_text('Card 1 on List 2')
        self.assertTrue(json_exported_page.is_json(), 'ERROR! the exported page does not look like JSON')
        json_exported_page.unexport_from_json()
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_drag_and_drop_to_another_list(self):
        logging.info('TEST CASE started: dragging a card from one list to a neighboring one')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.list1.drag_card_with_text_in_place_of_card_with_text('Card 6 on List 1', self.list2, 'Card 3 on List 2')
        self.assertTrue(self.list1.check_if_card_with_text_was_dropped_in_place_of_card_with_text('Card 6 on List 1', self.list2), 'ERROR! the card was not dragged and dropped')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_add_checklist(self):
        logging.info('TEST CASE started: adding a checklist to a card')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        checklist = self.list1.add_checklist_to_card_with_text('Card 5 on List 1', 'ASAP')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.assertTrue(checklist.check_if_list_has_title('ASAP'), 'ERROR! checklist was not created')
        checklist.delete()
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_add_items(self):
        logging.info('TEST CASE started: adding items to a checklist')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        checklist = self.list1.add_checklist_to_card_with_text('Card 3 on List 1', 'Not so ASAP')
        for number in range(1, 5):
            with_this_text = 'Urgent item ' + str(number)
            checklist.add_item(with_this_text)
        self.assertEqual(checklist.count_items(), len(checklist.items), 'ERROR! there are fewer items in the checklist')
        checklist.delete()
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_make_progress(self):
        logging.info('TEST CASE started: adding items to a checklist')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        checklist = self.list3.add_checklist_to_card_with_text('Card 2 on List 3', 'Not so ASAP')
        for number in range(1, 5):
            with_this_text = 'Urgent item ' + str(number)
            checklist.add_item(with_this_text)
        checklist.complete_task('Urgent item 3')
        checklist.complete_task('Urgent item 1')
        self.assertTrue(checklist.check_progress_percentage(2), 'ERROR! The progress bar shows different completion percentage')
        checklist.delete()
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_label_card(self):
        logging.info('TEST CASE started: labelling a card')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.list3.label_card_with_text('Card 1 on List 3', 'orange')
        self.assertTrue(self.list3.check_if_card_with_text_was_labelled_with_color('Card 1 on List 3', 'orange'), 'ERROR! the card was not labelled')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_assign_member(self):
        logging.info('TEST CASE started: assigning a member to a card')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.list1.assign_member_to_card_with_text('Card 4 on List 1', 'Python Wsb')
        self.assertTrue(self.list1.check_if_member_was_assigned_to_card_with_text('Card 4 on List 1', 'Python Wsb'), 'ERROR! Member was not assigned')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_comment_card(self):
        logging.info('TEST CASE started: commenting a card')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.list3.comment_card_with_text('Card 3 on List 3', 'Test comment number 1')
        self.assertTrue(self.list3.check_if_card_with_text_has_comment('Card 3 on List 3', 'Test comment number 1'), 'ERROR! Comment was not inserted properly')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_comment_card_with_emoji(self):
        logging.info('TEST CASE started: commenting a card with emoji')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.list4.comment_card_with_text_with_emoji('Card 1 on List 4', 'FFS...', 'facepalm')
        self.assertTrue(self.list4.check_if_card_with_text_has_comment_with_emoji('Card 1 on List 4', 'FFS...', 'facepalm'), 'ERROR! Emoji was not inserted properly')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_remove_comment(self):
        logging.info('TEST CASE started: removing a comment')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.list4.delete_comment_from_card_with_text('Card 2 on List 4', 'This will be gone')
        self.assertTrue(self.list4.check_if_comment_was_removed_from_card_with_text('Card 2 on List 4', 'This will be gone'), 'ERROR! The comment is still available')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def tearDown(self):
        time.sleep(3)

    @classmethod
    def tearDownClass(cls):
        try:
            cls.group_board.archive_list_with_title('Test List 1')
            cls.group_board.archive_list_with_title('Test List 2')
            cls.group_board.archive_list_with_title('Test List 3')
            cls.group_board.archive_list_with_title('Test List 4')
            cls.group_board.delete_board()
            cls.group_board.log_out()
        except NoSuchElementException:
            logging.error('ERROR! Could not archive lists and delete the board')
        finally:
            cls.driver.quit()
            logging.info('Trello login test finished @ %s \n' % (strftime("%Y-%m-%dT%H_%M_%S", gmtime())))

if __name__ == '__main__':
    with open(os.devnull, 'w') as null_stream:
        runner = TextTestRunner(stream=null_stream)
        runner.resultclass = TestResult
        suite = TestLoader().loadTestsFromTestCase(CardManipulations)
        result = runner.run(suite)

        if not result.wasSuccessful():
            logging.error('The overall result of the test suite Trello card manipulations is FAILED')
        else:
            logging.info('The overall result of the test suite Trello card manipulations is PASSED')
        if result.errors:
            logging.error(result.errors)
        if result.failures:
            logging.error(result.failures)
