import unittest
import logging
from time import gmtime, strftime
from Utils import settings
from Utils import helpers
import time
import os
from unittest import TextTestRunner, TestResult, TestLoader
from selenium.common.exceptions import NoSuchElementException
from Objects.login_page import LoginPage


class BasicComponentsCreation(unittest.TestCase):
    logging_orchestrator = helpers.TestArtifactsHelpers()
    logging_orchestrator.set_up_logging(settings.LOG_DIR, 'AddingElements')

    def setUp(self):
        self.driver = settings.BROWSER()
        logging.info('\nThe current test run has been performed on %s' % self.driver.capabilities['browserName'])
        login_page = LoginPage(self.driver)
        logged_dashboard = login_page.log_in(settings.VALID_USER, settings.VALID_PASSWORD)
        self.assertTrue(logged_dashboard.is_create_board_element_present(), 'ERROR! The board is not available for selection')
        self.group_board = logged_dashboard.create_new_group_board(settings.USER_GROUP, 'Testing 1')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_add_lists(self):
        logging.info('TEST CASE started: adding lists')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.group_board.add_list('Testing 1')
        self.group_board.add_list('Testing 2')
        self.assertEqual(self.group_board.count_lists(), len(self.group_board.lists), 'ERROR! The number of lists requested is different than those actually created')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def test_add_card(self):
        logging.info('TEST CASE started: adding lists')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        self.group_board.add_list('Testing 1')
        self.group_board.add_list('Testing 2')
        self.assertEqual(self.group_board.count_lists(), len(self.group_board.lists), 'ERROR! The number of lists requested is different than those actually created')
        list1 = self.group_board.find_list_by_title('Testing 1')
        for number in range(0, 6):
            text = 'Card ' + str(number + 1)
            list1.add_card(text)
        self.assertEqual(list1.count_cards(), len(list1.cards), 'ERROR! The number of cards requested is different than those actually created')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        list2 = self.group_board.find_list_by_title('Testing 2')
        for number in range(0, 3):
            text = 'Card ' + str(number + 1)
            list2.add_card(text)
        self.assertEqual(list2.count_cards(), len(list2.cards), 'ERROR! The number of cards requested is different than those actually created')
        helpers.TestArtifactsHelpers.get_resources_statistics()

    def tearDown(self):
        try:
            self.group_board.delete_board()
            self.group_board.log_out()
        except NoSuchElementException:
            logging.error('ERROR! Could not archive lists and delete the board')
        finally:
            self.driver.quit()
            logging.info('Trello login test finished @ %s \n' % (strftime("%Y-%m-%dT%H_%M_%S", gmtime())))
            time.sleep(5)


if __name__ == '__main__':
    with open(os.devnull, 'w') as null_stream:
        runner = TextTestRunner(stream=null_stream)
        runner.resultclass = TestResult
        suite = TestLoader().loadTestsFromTestCase(BasicComponentsCreation)
        result = runner.run(suite)

        if not result.wasSuccessful():
            logging.error('The overall result of the test suite Trello list and card adding is FAILED')
        else:
            logging.info('The overall result of the test suite Trello list and card adding is PASSED')
        if result.errors:
            logging.error(result.errors)
        if result.failures:
            logging.error(result.failures)

