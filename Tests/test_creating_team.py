import unittest
import logging
from time import gmtime, strftime
from Utils import settings, helpers
from Objects.login_page import LoginPage
import os
from unittest import TextTestRunner, TestResult, TestLoader
import time


class TeamOperations(unittest.TestCase):
    logging_orchestrator = helpers.TestArtifactsHelpers()
    logging_orchestrator.set_up_logging(settings.LOG_DIR, 'TeamOperations')

    def setUp(self):
        self.driver = settings.BROWSER()
        logging.info('\nThe current test run has been performed on %s' % self.driver.capabilities['browserName'])
        login_page = LoginPage(self.driver)
        self.logged_dashboard_page = login_page.log_in(settings.VALID_USER, settings.VALID_PASSWORD)
        self.assertTrue(self.logged_dashboard_page.is_create_board_element_present(), 'ERROR! Not ready to proceed yet!')
        self.creation_menu = self.logged_dashboard_page.open_plus_menu()

    def test_adding_new_team(self):
        logging.info('TEST CASE started: adding new team')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        popup = self.creation_menu.open_personal_team_creation_page(settings.NEW_TEAM)
        self.new_team_profile = popup.create_team(settings.NEW_TEAM)
        self.assertTrue(self.new_team_profile.is_team_created(), 'ERROR! Team has not been created')

    def tearDown(self):
        self.new_team_profile.delete()
        self.logged_dashboard_page.log_out()
        self.driver.quit()
        logging.info('Trello login test finished @ %s \n' % (strftime("%Y-%m-%dT%H_%M_%S", gmtime())))
        time.sleep(5)

if __name__ == '__main__':
    with open(os.devnull, 'w') as null_stream:
        runner = TextTestRunner(stream=null_stream)
        runner.resultclass = TestResult
        suite = TestLoader().loadTestsFromTestCase(TeamOperations)
        result = runner.run(suite)

        if not result.wasSuccessful():
            logging.error('The overall result of the test suite Trello logins is FAILED')
        else:
            logging.info('The overall result of the test suite Trello logins is PASSED')
        if result.errors:
            logging.error(result.errors)
        if result.failures:
            logging.error(result.failures)
