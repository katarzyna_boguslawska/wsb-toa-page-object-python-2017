import unittest
import logging
from time import gmtime
from Utils import settings
from Utils import helpers
from Objects.login_page import LoginPage
from Objects.logged_user_dashboard_page import LoggedUserDashboard
from selenium.webdriver.common.by import By
from unittest import TextTestRunner, TestResult, TestLoader
import os
import time


class CreatingBoard(unittest.TestCase):
    logging_orchestrator = helpers.TestArtifactsHelpers()
    logging_orchestrator.set_up_logging(settings.LOG_DIR, 'BoardManipulations')

    def setUp(self):
        self.driver = settings.BROWSER()
        logging.info('\nThe current test run has been performed on %s' % self.driver)
        login_page = LoginPage(self.driver)
        login_page.navigate()
        logged_dashboard = login_page.log_in(settings.VALID_USER, settings.VALID_PASSWORD)
        self.logged_dashboard = LoggedUserDashboard(self.driver)
        self.assertTrue(logged_dashboard.is_create_board_element_present(), 'ERROR! The board is not available for selection')

    def test_create_new_board(self):
        logging.info('TEST CASE started: creating a new board')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        personal_board = self.logged_dashboard.create_new_personal_board("My board")
        self.assertTrue(personal_board.is_board_visible("My board"), 'ERROR! The board was not created')
        personal_board.delete_board()

    def test_search_for_board(self):
        logging.info('TEST CASE started: searching for the new board')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        personal_board = self.logged_dashboard.create_new_personal_board("My board")
        personal_board.is_board_visible("My board")
        search_results = personal_board.search("My board")
        self.assertTrue(search_results.is_result_found(), 'ERROR! No matching search result')
        search_results.go_to_board("My board")
        personal_board.delete_board()

    def test_rename_board(self):
        logging.info('TEST CASE started: changing the name')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        helpers.TestArtifactsHelpers.get_resources_statistics()
        personal_board = self.logged_dashboard.create_new_personal_board("My board")
        personal_board.rename("My renamed board")
        self.assertTrue(personal_board.is_board_visible("My renamed board"), 'ERROR! Renamed board not displayed')
        personal_board.delete_board()

    def test_delete_the_board(self):
        logging.info('TEST CASE started: deleting the board')
        helpers.TestArtifactsHelpers.get_resources_statistics()
        personal_board = self.logged_dashboard.create_new_personal_board("My board")
        personal_board.delete_board()
        self.assertFalse(personal_board.is_board_visible("My board"), 'ERROR! The board was not deleted')

    def tearDown(self):
        logging.info('Trello login test finished @ %s \n' % gmtime())
        self.logged_dashboard.log_out()
        self.driver.quit()
        time.sleep(5)

if __name__ == "__main__":
    with open(os.devnull, 'w') as null_stream:
        runner = TextTestRunner(stream=null_stream)
        runner.resultclass = TestResult
        suite = TestLoader().loadTestsFromTestCase(CreatingBoard)
        result = runner.run(suite)

        if not result.wasSuccessful():
            logging.error('The overall result of the test suite Trello card manipulations is FAILED')
        else:
            logging.info('The overall result of the test suite Trello card manipulations is PASSED')
        if result.errors:
            logging.error(result.errors)
        if result.failures:
            logging.error(result.failures)
