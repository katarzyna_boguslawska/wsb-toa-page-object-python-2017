import schedule
import time
import test_trello


def test_job():
    test_trello.check_and_run_tests()

schedule.every(2).minutes.do(test_job)


while True:
    schedule.run_pending()
    time.sleep(1)
