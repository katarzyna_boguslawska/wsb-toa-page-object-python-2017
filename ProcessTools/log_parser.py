from time import gmtime, strftime
import os
import shutil
import settings
import re


class LogParser(object):

    @staticmethod
    def get_logfile_name(path):
        """Returns the name of the last log file"""
        all_filenames = os.listdir(path)
        all_files = []
        for filename in all_filenames:
            plausible_log_file = os.path.join(path, filename)
            if os.path.isfile(plausible_log_file):
                all_files.append(plausible_log_file)
        sorted_files = sorted(all_files, key=os.path.getctime)
        return sorted_files[len(sorted_files) - 1]

    @staticmethod
    def archive_log(log_file):
        """Moves the log file into an archive to make room for next run"""
        shutil.move(log_file, settings.ARCHIVED_LOG_DIR)

    @staticmethod
    def is_overall_result_success(log_file):
        """Determine if the test run was passed based on occurace of a summary phrase"""
        if settings.SUCCESSFUL_SUMMARY in open(log_file).read():
            return True
        else:
            return False

    @staticmethod
    def get_cpu_avarage(log_file):
        """Collect all probes of CPU consumption"""
        cpu_entry_marker = 'INFO:root:Current CPU usage percentage is'
        cpu_probes = []
        with open(log_file, 'r') as what_was_logged:
            text = what_was_logged.readlines()
            for line in text:
                if cpu_entry_marker in line:
                    probe = float(line[41:].strip())
                    if probe != 0.0:
                        cpu_probes.append(probe)
        avarage_cpu_consumption = sum(cpu_probes) / len(cpu_probes)
        return round(avarage_cpu_consumption, 2)

    @staticmethod
    def get_memory_avarage(log_file):
        """Collect all probes of memory consumption"""
        memory_entry_marker = 'INFO:root:Current memory consumption is'
        memory_probes = []
        with open(log_file, 'r') as what_was_logged:
            text = what_was_logged.readlines()
            for line in text:
                if memory_entry_marker in line:
                    probe = float(line[40:-3].strip())
                    memory_probes.append(probe)
        avarage_memory_consumption = sum(memory_probes) / len(memory_probes)
        return round(avarage_memory_consumption, 2)

    @staticmethod
    def append_resource_avarage_log(log_file):
        """Add the avarge CPU and mem consumption summary to the log file"""
        cpu = LogParser.get_cpu_avarage(log_file)
        mem = LogParser.get_memory_avarage(log_file)
        cpu_entry = 'The avarage CPU consumption throghout the test run was %s' % cpu
        mem_entry = 'The avarage memory consumption throghout the test run was %s MB' % mem
        with open(log_file, 'a') as what_was_already_logged:
            what_was_already_logged.write(cpu_entry + '\n')
            what_was_already_logged.write(mem_entry + '\n')

    @staticmethod
    def get_successes(log_file):
        """Collect names of all passed test cases"""
        test_beginning_marker = 'INFO:root:TEST CASE started:'
        passed = 'passed'
        results = {}
        with open(log_file, 'r') as what_was_logged:
            text = what_was_logged.readlines()
            for line in text:
                if test_beginning_marker in line:
                    testname = line[28:].strip()
                    results[testname] = passed
        return results

    @staticmethod
    def get_errors(log_file):
        """Collect names of all failed test cases"""
        failed = 'failed'
        results = {}
        errors = []
        with open(log_file, 'r') as what_was_logged:
            text = what_was_logged.readlines()
            for line in text:
                pattern = ur"[a-zA-Z0-9_=]+\>"
                matches = re.findall(pattern, line)
                for match in matches:
                    is_nonwhitespace = not match.startswith(' ')
                    if is_nonwhitespace and match not in errors:
                        errors.append(match)
        for error in errors:
            actual_testname = error[16:-1]
            pretty_testname = actual_testname.replace('_', ' ')
            results[pretty_testname] = failed
        return results

    @staticmethod
    def get_all_results(log_file):
        """Combine all results into a single JSON"""
        passed = LogParser.get_successes(log_file)
        failed = LogParser.get_errors(log_file)
        all_testcases = passed.keys() + failed.keys()
        archived_logs_path = os.path.join(settings.ARCHIVED_LOG_DIR, os.path.basename(log_file))
        logs = {}
        test_outcome = {}
        passed.update(failed)
        for testcase in all_testcases:
            logs[testcase] = archived_logs_path
        test_outcome['date'] = strftime("%Y-%m-%d", gmtime())
        test_outcome['results'] = passed
        test_outcome['logs'] = logs
        return test_outcome

    @staticmethod
    def parse_logs():
        """Main method of module"""
        the_file = LogParser.get_logfile_name(settings.LOG_DIR)
        complete_json = LogParser.get_all_results(the_file)
        LogParser.append_resource_avarage_log(the_file)
        LogParser.archive_log(the_file)
        return complete_json


if __name__ == '__main__':

    LogParser.parse_logs()

