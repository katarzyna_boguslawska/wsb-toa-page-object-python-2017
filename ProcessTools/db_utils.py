from pymongo import MongoClient
import settings


def get_last_runID():
    connection = MongoClient(settings.URI_READ)
    db = connection.testTrello
    latest_run_result = db.testTrello.find().sort('runID', -1).limit(1)
    connection.close()
    last_run_id = int(latest_run_result[0]['runID'])
    return last_run_id


def prepare_complete_json(json):
    last_run_id = get_last_runID()
    json['runID'] = last_run_id + 1
    return json


def insert_test_results(json):
    complete_json = prepare_complete_json(json)
    connection = MongoClient(settings.URI_READ_WRITE)
    db = connection.testTrello
    db.testTrello.insert_one(complete_json)
    connection.close()
