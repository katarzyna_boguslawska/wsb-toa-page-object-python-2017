import imaplib
import smtplib
from email.parser import Parser
from email.mime.text import MIMEText
from optparse import OptionParser
import settings


def read_emails(login, password, server, subject_pattern):
    """Read emails from a particular mailbox"""
    imap_connection = imaplib.IMAP4_SSL(server)
    imap_connection.login(login, password)
    imap_connection.select('TRIGGERRED')
    status, data = imap_connection.search(None, '(UNSEEN)')
    trigger = None
    if status == 'OK' and data[0] != '':
        for email in data[0].split():
            fetch_status, data = imap_connection.fetch(email, '(RFC822)')
            headers = Parser().parsestr(data[0][1])
            wanted_element = headers['subject']
            if wanted_element == subject_pattern:
                trigger = True
            else:
                trigger = False
        imap_connection.close()
        imap_connection.logout()
    else:
        trigger = False
        imap_connection.close()
        imap_connection.logout()
    return trigger


def send_mail(html, recipient, login, password, server, port, subject):
    """Send email to the specified recipient"""
    message = MIMEText(html, 'html')
    message['From'] = 'Python WSB<'+login+'>'
    message['To'] = recipient
    message['Subject'] = subject

    smtp = smtplib.SMTP_SSL(server, port)
    smtp.ehlo()
    smtp.login(login, password)
    smtp.sendmail(message['From'], message['To'], message.as_string())
    smtp.quit()


if __name__ == '__main__':
    send_failure = None
    send_success = None
    trigger = None
    read = None

    parser = OptionParser()
    parser.add_option("-f", "--failed", action="store_true", dest="send_failure", help="send email about test run failed")
    parser.add_option("-p", "--passed", action="store_true", dest="send_success", help="send email about test run passed")
    parser.add_option("-t", "--trigger", action="store_true", dest="trigger", help="send email to trigger a test run")
    parser.add_option("-r", "--read", action="store_true", dest="read", help="read mailbox for triggers")

    (options, args) = parser.parse_args()

    if options.send_failure:
        send_mail(settings.FAILURE_TEMPLATE, settings.LOGIN, settings.LOGIN, settings.PASSWORD, settings.SERVER, settings.PORT, 'Automated Trello test results - FAILED')
    elif options.send_success:
        send_mail(settings.SUCCESS_TEMPLATE, settings.LOGIN, settings.LOGIN, settings.PASSWORD, settings.SERVER, settings.PORT, 'Automated Trello test results - PASSED')
    elif options.trigger:
        send_mail(settings.TRIGGER_TEMPLATE, settings.LOGIN, settings.LOGIN, settings.PASSWORD, settings.SERVER, settings.PORT, '[TEST.TRIGGER]Test are about to start')
    elif options.read:
        is_test_start = read_emails(settings.LOGIN, settings.PASSWORD, settings.SERVER, settings.SUBJECT)
        print is_test_start



