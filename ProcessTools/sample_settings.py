''' MAIL VARIABLES '''
### CHANGE THEM TO MATCH YOUR EMAIL ACCOUNT CREDENTIALS - WON'T WORK OTHERWISE ###
LOGIN = 'mailbox_to_query@example.com'
PASSWORD = 'password123'
SERVER = 'www.example.com'
PORT = 123
SUBJECT = 'This subject should trigger tests'
LOG_DIR = '/home/me/my_selenium_logs'
ARCHIVED_LOG_DIR = '/home/me/my_selenium_logs/archived/'
SUCCESSFUL_SUMMARY = 'INFO:All tests passed'
FAILED_SUMMARY = 'ERROR:Tests failed'

SUCCESS_TEMPLATE = """\
<html>
  <head></head>
  <body>
    <p>Hello from Test Automation!<br>
       Test Automation Batman is happy to announce that your tests have completed and their result is <b><font color="#008000">PASSED</font></b><br>
       You can view the logs or see the results in context at:<br>
       <a href="http://localhost:port">link</a>
    </p>
    <p>Cheers!<br>
    Batman</p>
  </body>
</html>
"""

FAILURE_TEMPLATE = """\
<html>
  <head></head>
  <body>
    <p>Hello from Test Automation!<br>
       Test Automation Batman regrets to announce that your tests have completed but their result is <b><font color="#ff4040">FAILED</font>.<b><br>
       You can view the logs or see the results in context at:<br>
       <a href="http://localhost:port">link</a>
    </p>
    <p>Never give up!<br>
    Batman</p>
  </body>
</html>
"""

TRIGGER_TEMPLATE = """\
<html>
  <head></head>
  <body>
    <p>Hello from Test Automation!<br>
       Test Automation Batman is pleased to announce that your tests are about to start. You will be informed about the result with a separate email.<br>
       Meanwhile keep calm and do not call Batman
    </p>
    <p>Cheers!<br>
    Batman</p>
  </body>
</html>
"""

''' DATABASE VARIABLES '''
### CHANGE IT TO MATCH YOUR ACTUAL DB CREDENTIALS - WON'T WORK OTHERWISE ###
URI_READ = 'mongodb://read_uer:read_password@localhost:27017/test_results_db'
URI_READ_WRITE = 'mongodb://write_uer:write_password@localhost:27017/test_results_db'

''' TEST VARIABLES '''
### CHANGE IT TO MATCH YOUR EXISTING DIRECTORY WITH TESTS - WON'T WORK OTHERWISE ###
TEST_REPOSITORY = '/home/me/my_selenium_tests'
