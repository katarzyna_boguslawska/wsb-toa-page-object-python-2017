from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.logged_user_dashboard_page import LoggedUserDashboard
from Utils import settings


class LoginPage(BasePage):

    title_locator = '//h1'
    username_field_locator = '//input[@id="user"]'
    password_field_locator = '//input[@id="password"]'
    login_button_locator = '//input[@id="login"]'
    password_reset_link_locator = '//a[@href="/forgot"]'

    # locators applicable to the 'new' login page (the one with Taco)
    t_username_field_locator = '//input[@id="login-identifier"]'
    t_confirm_locator = '//button[@class="button button-green js-login-button"]'
    t_login_button_locator = '//button[@class="button button-green js-password-button"]'
    t_failure_locator = '//p[@class="error-message"]'

    def __init__(self, driver):
        BasePage.__init__(self, driver)
        self.url = settings.VALID_LOGIN_URL
        self.driver = driver
        self.navigate()

    # verifies if the page you're at contains the text "Log in to Trello" in the title
    def is_title_matches(self):
        title = self.get_element(By.XPATH, self.title_locator)
        eng_title = 'Log in to Trello'
        pl_title = 'Zaloguj'
        if eng_title in title.text or pl_title in title.text:
            return True
        else:
            return False

    # finds the elements and completes them
    def complete_login_credentials(self, login, password):
        try:
            username_field = self.get_element(By.XPATH, self.username_field_locator)
            username_field.type(login)
            username_field.click_enter()
            self.get_element(By.XPATH, self.password_field_locator).type(password)
        except TimeoutException:
            username_field = self.get_element(By.XPATH, self.t_username_field_locator)
            if self.is_clickable(By.XPATH, self.t_username_field_locator):
                username_field.focus_and_type(login)
            confirm_button = self.get_element(By.XPATH, self.t_confirm_locator)
            if self.is_clickable(By.XPATH, self.t_confirm_locator):
                confirm_button.click()
            password_field = self.get_element(By.XPATH, self.password_field_locator)
            if self.is_clickable(By.XPATH, self.password_field_locator):
                password_field.type(password)

    # finds the elements and completes them
    def complete_login(self, login):
        try:
            username_field = self.get_element(By.XPATH, self.username_field_locator)
            username_field.type(login)
            username_field.click_enter()
        except TimeoutException:
            identifier_field = self.get_element(By.XPATH, self.t_username_field_locator)
            if self.is_clickable(By.XPATH, self.t_username_field_locator):
                identifier_field.focus_and_type(login)
            confirm_identifier_button = self.get_element(By.XPATH, self.t_confirm_locator)
            if self.is_clickable(By.XPATH, self.t_confirm_locator):
                confirm_identifier_button.click()

        # finds the elements and completes them
    def complete_password(self, password):
        self.get_element(By.XPATH, self.password_field_locator).focus_and_type(password)

    # finds the submit button and clicks it
    def click_submit_button(self):
        try:
            self.get_element(By.XPATH, self.login_button_locator).click()
        except TimeoutException:
            submit_button = self.get_element(By.XPATH, self.t_login_button_locator)
            if self.is_clickable(By.XPATH, self.t_login_button_locator):
                submit_button.click()
        return LoggedUserDashboard(self.driver)

    def ask_for_password_reset(self, login):
        try:
            self.get_element(By.XPATH, self.t_username_field_locator).focus_and_type(login).click()
            self.get_element(By.XPATH, self.t_confirm_locator).click()
        except TimeoutException:
            self.is_visible(By.XPATH, self.password_reset_link_locator)
        password_reset_link = self.get_element(By.XPATH, self.password_reset_link_locator)
        self.is_clickable(By.XPATH, self.password_reset_link_locator)
        password_reset_link.click()
        return PasswordResetPage(self.driver)

    def log_in(self, username, password):
        self.complete_login_credentials(username, password)
        self.click_submit_button()
        return LoggedUserDashboard(self.driver)

    def fail_login(self):
        try:
            self.get_element(By.XPATH, self.login_button_locator).click()
        except TimeoutException:
            self.get_element(By.XPATH, self.t_login_button_locator).click()
        return LoginFailurePage(self.driver)

    def is_error_communicated(self):
        if self.is_present(By.XPATH, self.t_failure_locator):
            return True
        else:
            return False


class LoginFailurePage(BasePage):

    failure_message_element_locator = '//div[@id="error"]'
    t_failure_locator = '//p[@class="error-message"]'

    def __init__(self, driver):
        BasePage.__init__(self, driver)
        self.driver = driver

    def is_error_communicated(self):
        if self.is_present(By.XPATH, self.failure_message_element_locator):
            return True
        elif self.is_present(By.XPATH, self.t_failure_locator):
            return True
        else:
            return False


class PasswordResetPage(BasePage):

    email_input = '//input[@id="email"]'
    submit_button = '//input[@id="submit"]'
    reset_heading = '//h1'
    reset_details = '//p[@id="feedback"]'

    t_email_input = '//input[@id="forgot-email"]'
    t_submit_button = '//button[@class="button button-green js-forgot-button"]'
    t_email_notification = '//p[@class="js-check-email-text text-center"]'

    def __init__(self, driver):
        BasePage.__init__(self, driver)
        self.driver = driver
        self.url = settings.PASSWORD_RESET

    def complete_credentials_for_password_reset(self):
        try:
            self.get_element(By.XPATH, self.email_input).type(settings.VALID_USER)
            self.get_element(By.XPATH, self.submit_button).click()
        except TimeoutException:
            self.get_element(By.XPATH, self.t_submit_button).click()

    def is_email_sent(self):
        is_heading_available = self.is_present(By.XPATH, self.reset_heading)
        is_details_available = self.is_present(By.XPATH, self.reset_details)
        is_email_sent = self.is_present(By.XPATH, self.t_email_notification)
        basic_condition = is_heading_available and is_details_available
        t_condition = is_email_sent
        if basic_condition or t_condition:
            return True
        else:
            return False
