from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Extensions.generic_logged_in_actions import GenericLoggedInAction


class JsonExportedPage(GenericLoggedInAction, BasePage):

    def __init__(self, driver, card):
        BasePage.__init__(self, driver)
        self.generic_actions_manager = GenericLoggedInAction(driver)
        self.driver = driver
        self.card = card

    def is_json(self):
        body = self.get_element(By.XPATH, '//body')
        return body.text.startswith('{\"id\":\"')

    def unexport_from_json(self):
        self.driver.back()
        self.card.close()

    def log_out(self):
        self.generic_actions_manager.log_out()

    def go_to_main_dashboard(self):
        self.generic_actions_manager.go_to_main_dashboard()

    def search(self, what):
        return self.generic_actions_manager.search(what)
