import time

from selenium.common.exceptions import NoSuchElementException, TimeoutException, StaleElementReferenceException
from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait

from Objects.Abstractions.base_element import BaseElement
from Utils import settings


class BasePage(object):

    url = None

    def __init__(self, driver):
        self.driver = driver
        self.driver.maximize_window()

    def navigate(self):
        self.driver.get(self.url)

    def is_present(self, by_what_locator, which_element, reference_element_by=None, reference_element_locator=None, timeout=settings.GENERAL_TIMEOUT):
        if not reference_element_locator:
            try:
                is_available = ec.presence_of_element_located((by_what_locator, which_element))
                WebDriverWait(self.driver, timeout).until(is_available)
                return True
            except TimeoutException:
                return False
        else:
            how_many_attempts = int(timeout / 0.2)
            attempt = 0
            while attempt < how_many_attempts:
                try:
                    attempt += 1
                    self.get_element(reference_element_by, reference_element_locator).find_element(by_what_locator, which_element)
                    return True
                except (NoSuchElementException, StaleElementReferenceException):
                    time.sleep(0.2)
            return False

    def is_visible(self, by_what_locator, which_element, reference_element_by=None, reference_element_locator=None, timeout=settings.GENERAL_TIMEOUT):
        if not reference_element_locator:
            try:
                is_available = ec.visibility_of_element_located((by_what_locator, which_element))
                WebDriverWait(self.driver, timeout).until(is_available)
                return True
            except TimeoutException:
                return False
        else:
            is_present = self.is_present(by_what_locator, which_element, reference_element_by, reference_element_locator, timeout)
            if is_present:
                return self.get_element(reference_element_by, reference_element_locator).find_element(by_what_locator, which_element).is_displayed()
            else:
                return False

    def is_clickable(self, by_what_locator, which_element, timeout=settings.GENERAL_TIMEOUT):
        try:
            is_available = ec.element_to_be_clickable((by_what_locator, which_element))
            WebDriverWait(self.driver, timeout).until(is_available)
            return True
        except TimeoutException:
            return False

    def is_absent(self, by_what_locator, which_element, reference_element_by=None, reference_element_locator=None, timeout=settings.GENERAL_TIMEOUT):
        return not self.is_present(by_what_locator, which_element, reference_element_by, reference_element_locator, timeout)

    def get_element(self, by_what_locator, which_element, reference_element_by=None, reference_element_locator=None, timeout=settings.GENERAL_TIMEOUT):
        root = None
        if isinstance(reference_element_by, BaseElement):
            how_many_attempts = int(timeout / 0.2)
            for attempt in range(0, how_many_attempts):
                try:
                    attempt += 1
                    root = reference_element_by.find_element(by_what_locator, which_element)
                    return BaseElement(self.driver, root, by_what_locator, which_element, reference_element_by.locator_type, reference_element_by.xpath)
                except (NoSuchElementException, StaleElementReferenceException):
                    time.sleep(0.2)
        elif reference_element_locator:
            how_many_attempts = int(timeout / 0.2)
            for attempt in range(0, how_many_attempts):
                try:
                    attempt += 1
                    root = self.get_element(reference_element_by, reference_element_locator).find_element(by_what_locator, which_element)
                    return BaseElement(self.driver, root, by_what_locator, which_element, reference_element_by, reference_element_locator)
                except (NoSuchElementException, StaleElementReferenceException):
                    time.sleep(0.2)
        else:
            is_available = ec.presence_of_element_located((by_what_locator, which_element))
            root = WebDriverWait(self.driver, timeout).until(is_available)
            return BaseElement(self.driver, root, by_what_locator, which_element, reference_element_by, reference_element_locator)

    def get_elements(self, by_what_locator, which_element, reference_element_by=None, reference_element_locator=None, timeout=settings.GENERAL_TIMEOUT):
        if isinstance(reference_element_by, BaseElement):
            if self.is_visible(by_what_locator, which_element, reference_element_by, reference_element_locator, timeout):
                base_elements = []
                web_elements = reference_element_by.find_elements(by_what_locator, which_element)
                for web_element in web_elements:
                    base_elements.append(BaseElement(self.driver, web_element, by_what_locator, which_element, reference_element_by.locator_type, reference_element_by.xpath))
                return base_elements
            else:
                raise NoSuchElementException
        elif reference_element_locator:
            if self.is_visible(by_what_locator, which_element, reference_element_by, reference_element_locator, timeout):
                base_elements = []
                web_elements = self.get_element(reference_element_by, reference_element_locator).find_elements(by_what_locator, which_element)
                for web_element in web_elements:
                    base_elements.append(BaseElement(self.driver, web_element, by_what_locator, which_element, reference_element_by, reference_element_locator))
                return base_elements
            else:
                raise NoSuchElementException
        else:
            if self.is_visible(by_what_locator, which_element):
                base_elements = []
                web_elements = self.driver.find_elements(by_what_locator, which_element)
                for web_element in web_elements:
                    base_elements.append(BaseElement(self.driver, web_element, by_what_locator, which_element, reference_element_by, reference_element_locator))
                return base_elements
            else:
                return None

    def is_stable(self, element):
        try:
            ActionChains(self.driver).move_to_element(element).perform()
            return True
        except (StaleElementReferenceException, NoSuchElementException, AttributeError):
            return False
