from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException, StaleElementReferenceException
import time
from Utils import settings


class BaseElement(WebElement):

    def __init__(self, driver, root, locator_type=None, xpath=None, reference_element_by=None, reference_element_locator=None):
        WebElement.__init__(self, root.parent, root.id)
        self.driver = driver
        self.root = root
        self.locator_type = locator_type
        self.xpath = xpath
        self.reference_element_by = reference_element_by
        self.reference_element_locator = reference_element_locator

    def drag_to(self, destination):
        self.repeat(self.essential_drag_to, settings.GENERAL_TIMEOUT, destination)

    def essential_drag_to(self, destination):
        ActionChains(self.driver).drag_and_drop(self.root, destination).perform()

    def focus_and_type(self, text):
        self.repeat(self.essential_focus_and_type, settings.GENERAL_TIMEOUT, text)
        return self

    def essential_focus_and_type(self, text):
        self.root.clear()
        ActionChains(self.driver).move_to_element(self.root).click().send_keys(text).perform()

    def type(self, text):
        self.repeat(self.essential_type, settings.GENERAL_TIMEOUT, text)
        return self

    def essential_type(self, text):
        self.root.clear()
        ActionChains(self.driver).move_to_element(self.root).send_keys(text).perform()

    def click(self):
        self.repeat(self.essential_click, settings.GENERAL_TIMEOUT)
        return self

    def essential_click(self):
        ActionChains(self.driver).move_to_element(self.root).click().perform()

    def click_enter(self):
        self.repeat(self.essential_click_enter, settings.GENERAL_TIMEOUT)

    def essential_click_enter(self):
        self.root.send_keys(Keys.ENTER)

    def click_tab(self):
        self.repeat(self.essential_click_tab, settings.GENERAL_TIMEOUT)

    def essential_click_tab(self):
        self.root.send_keys(Keys.TAB)

    def clear(self):
        self.repeat(self.essenial_clear, settings.GENERAL_TIMEOUT)
        return self

    def essenial_clear(self):
        self.root.clear()

    def submit(self):
        self.repeat(self.essential_submit, settings.GENERAL_TIMEOUT)

    def essential_submit(self):
        self.root.submit()

    def get_tag_name(self):
        return self.root.get_tag_name()

    def get_attribute(self, name):
        return self.root.get_attribute(name)

    def is_selected(self):
        return self.root.is_selected()

    def is_displayed(self):
        return self.root.is_displayed

    def is_enabled(self):
        return self.root.is_enabled()

    def get_property(self, name):
        return self.root.get_property(name)

    def find_element(self, by=By.ID, value=None):
        return self.root.find_element(by, value)

    def find_elements(self, by=By.ID, value=None):
        return self.root.find_elements(by, value)

    def get_location(self):
        return self.root.location()

    def stabilize(self, element):
        ActionChains(self.driver).move_to_element(element.root).perform()
        return element

    def is_stable(self, element):
        try:
            ActionChains(self.driver).move_to_element(element.root).perform()
            return True
        except StaleElementReferenceException:
            return False

    def repeat(self, what_action, timeout, parameter=None):
        how_many_attempts = int(timeout / 0.5)
        attempt = 0
        while attempt <= how_many_attempts:
            attempt += 1
            is_stable = self.is_stable(self)
            if is_stable:
                break
        if not parameter:
            how_many_attempts = int(timeout / 0.5)
            attempt = 0
            is_continue = True
            while attempt <= how_many_attempts and is_continue:
                try:
                    attempt += 1
                    what_action()
                    is_continue = False
                except StaleElementReferenceException:
                    time.sleep(0.5)
        else:
            how_many_attempts = int(timeout / 0.5)
            attempt = 0
            is_continue = True
            while attempt <= how_many_attempts and is_continue:
                try:
                    attempt += 1
                    what_action(parameter)
                    is_continue = False
                except StaleElementReferenceException:
                    time.sleep(0.5)

