import time
from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Components.BoardMenu.board_menu import BoardMenu
from Objects.Components.list_page import List
from Objects.Interfaces.i_boardable import IBoardable
from Utils import settings


class PersonalDashboard(IBoardable, BasePage):

    list_title_input_locator = '//input[@class="list-name-input"]'
    save_list_locator = '//input[@class="primary mod-list-add-button js-save-edit"]'
    show_menu_locator = '//a[@class="board-header-btn mod-show-menu js-show-sidebar"]'
    generic_list_locator = '//div[@class="js-list list-wrapper"]'
    background_css_locator = "/html/body"
    name_button_locator = '//a[@class="board-header-btn board-header-btn-name js-rename-board"]'
    new_board_name_input = '//input[@class="js-board-name js-autofocus"]'
    confirm_renaming_button = '//input[@class="primary wide js-rename-board" and @value="Rename"]'
    board_menu_area_locator = '//div[@class="board-menu-container"]'

    def __init__(self, driver, generic_actions_manager):
        BasePage.__init__(self, driver)
        self.generic_actions_manager = generic_actions_manager
        self.driver = driver
        self.lists = []

    def is_board_visible(self, title):
        board_title_locator = '//span[@class="board-header-btn-text" and text()="' + title + '"]'
        return self.is_visible(By.XPATH, board_title_locator)

    def create_list(self, list_title):
        self.get_element(By.XPATH, self.list_title_input_locator).focus_and_type(list_title)
        self.get_element(By.XPATH, self.save_list_locator).click()
        parent = '//div[@class="js-list list-wrapper" and .//textarea[text()="' + list_title + '"]]'
        return List(self.driver, parent, list_title)

    def add_list(self, list_title):
        trello_list = self.create_list(list_title)
        self.lists.append(trello_list)

    def find_list_by_title(self, title):
        for trello_list in self.lists:
            if trello_list.title == title:
                return trello_list

    def archive_list_with_title(self, list_title):
        list_to_archive = self.find_list_by_title(list_title)
        list_to_archive.archive()
        self.lists.remove(list_to_archive)

    def count_lists(self):
        all_lists_containers = self.get_elements(By.XPATH, self.generic_list_locator)
        if all_lists_containers:
            return len(all_lists_containers)
        else:
            return 0

    def show_menu(self):
        if self.is_visible(By.XPATH, self.board_menu_area_locator):
            return BoardMenu(self.driver)
        else:
            self.get_element(By.XPATH, self.show_menu_locator).click()
            return BoardMenu(self.driver)

    def delete_board(self):
        menu = self.show_menu()
        menu.delete_board()

    def get_background_color(self):
        element = self.get_element(By.XPATH, self.background_css_locator)
        return element.value_of_css_property("background-color")

    def check_if_background_is_photo(self):
        background = "none"
        how_many_attempts = int(settings.GENERAL_TIMEOUT / 0.2)
        for attempt in range(0, how_many_attempts):
            background = self.get_element(By.XPATH, self.background_css_locator).value_of_css_property("background-image")
            attempt += 1
            if background != "none":
                return True
            else:
                time.sleep(0.2)
        print background
        if background != "none":
            return True
        else:
            return False

    def log_out(self):
        self.generic_actions_manager.log_out()

    def go_to_main_dashboard(self):
        self.generic_actions_manager.go_to_main_dashboard()

    def search(self, what):
        return self.generic_actions_manager.search(what)

    def go_to_board(self, which_board):
        self.generic_actions_manager.go_to_board(which_board)

    def rename(self, to_what):
        self.get_element(By.XPATH, self.name_button_locator).click()
        self.get_element(By.XPATH, self.new_board_name_input).clear()
        self.get_element(By.XPATH, self.new_board_name_input).type(to_what)
        self.get_element(By.XPATH, self.confirm_renaming_button).click()
