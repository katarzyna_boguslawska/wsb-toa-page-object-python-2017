from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Components.GeneralMenus.CreationMenu import CreationMenu
from Objects.Components.GeneralMenus.UserMenu import UserMenu
from Objects.search_result_page import SearchResult


class GenericLoggedInAction:
    logo_locator = '//a[@class="header-logo js-home-via-logo"]'
    search_locator = '//input[@class="header-search-input js-search-input js-disable-on-dialog" and @type="text"]'
    search_button_locator = '//a[@class="header-search-icon header-search-icon-open icon-lg icon-external-link light js-open-button js-open-search-page header-search-icon-dark"]'
    boards_button_locator = '//a[@class="header-btn header-boards js-boards-menu"]'

    plus_locator = '//a[@class="header-btn js-open-add-menu"]'
    info_locator = '//a[@class="header-btn js-open-header-info-menu"]'
    notifications_locator = '//a[@class="header-btn header-notifications js-open-header-notifications-menu"]'
    user_settings_locator = '//a[@class="header-btn header-avatar js-open-header-member-menu"]'

    def __init__(self, driver):
        # type (WebDriver) -> GenericLoggedInAction
        self.base_page = BasePage(driver)
        self.driver = driver

    def log_out(self):
        # type () -> None
        user_menu = self.open_user_menu()
        user_menu.log_out()

    def open_plus_menu(self):
        # type () -> CreationMenu
        self.base_page.get_element(By.XPATH, self.plus_locator).click()
        return CreationMenu(self.driver, self)

    def open_user_menu(self):
        # type () -> UserMenu
        self.base_page.get_element(By.XPATH, self.user_settings_locator).click()
        return UserMenu(self.driver)

    def go_to_main_dashboard(self):
        # type () -> None
        self.base_page.get_element(By.XPATH, self.logo_locator).click()

    def search(self, what):
        # type (str) -> SearchResult
        self.base_page.get_element(By.XPATH, self.search_locator).focus_and_type(what)
        self.base_page.get_element(By.XPATH, self.search_button_locator).click()
        return SearchResult(self.driver, what, self)

    def open_boards_menu(self):
        # type () -> None
        self.base_page.get_element(By.XPATH, self.boards_button_locator).click()

    def go_to_board(self, which_board):
        # type (str) -> None
        self.open_boards_menu()
        self.base_page.get_element(By.XPATH, '//a[@class="js-open-board compact-board-tile-link dark" and .//span[@title="' + which_board + '"]]').click()
