from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Extensions.confirmation_page import ConfirmationPage
from Objects.Interfaces.i_closeable import ICloseable


class ClosePopup(BasePage, ICloseable):

    close_component_button_locator = '//input[@class="js-confirm full negate" and @type="submit"]'
    close_popup_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'

    def __init__(self, driver):
        # type (WebDriver) -> ClosePopup
        BasePage.__init__(self, driver)
        self.driver = driver

    def close_component(self):
        # type () -> ConfirmationPage
        self.get_element(By.XPATH, self.close_component_button_locator).click()
        return ConfirmationPage(self.driver)

    def close(self):
        # type () -> None
        self.get_element(By.XPATH, self.close_popup_locator).click()
