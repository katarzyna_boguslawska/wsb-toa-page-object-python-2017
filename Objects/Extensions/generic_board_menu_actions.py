from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable


class GenericBoardMenuAction(ICloseable):

    close_the_board_menu_locator = '//a[@class="board-menu-header-close-button icon-lg icon-close js-hide-sidebar"]'
    back_arrow_menu_button = '//a[@class="board-menu-header-back-button icon-lg icon-back js-pop-widget-view"]'

    def __init__(self, driver):
        # type (WebDriver) -> GenericLoggedInAction
        self.base_page = BasePage(driver)
        self.driver = driver

    def close(self):
        self.base_page.get_element(By.XPATH, self.close_the_board_menu_locator).click()

    def go_to_main_menu(self):
        is_continue = True
        while is_continue:
            if self.base_page.get_element(By.XPATH, self.back_arrow_menu_button):
                self.base_page.get_element(By.XPATH, self.back_arrow_menu_button).click()
            else:
                is_continue = False
