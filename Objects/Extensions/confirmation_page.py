from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage


class ConfirmationPage(BasePage):

    delete_locator = '//a[@class="quiet js-delete"]'
    reopen_locator = '//a[@class="js-reopen"]'

    def __init__(self, driver):
        # type (WebDriver) -> MoreSubmenu
        BasePage.__init__(self, driver)
        self.driver = driver

    def confirm_closing(self):
        # type () -> None
        self.get_element(By.XPATH, self.delete_locator).click()

    def reopen(self):
        # type () -> None
        self.get_element(By.XPATH, self.reopen_locator).click()
