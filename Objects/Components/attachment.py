from Objects.Abstractions.base_page import BasePage


class Attachment(BasePage):

    def __init__(self, driver, parent, link):
        # type (WebDriver, str, str) -> Attachment

        BasePage.__init__(self, driver)
        self.parent = parent
        self.driver = driver
        self.link = link
