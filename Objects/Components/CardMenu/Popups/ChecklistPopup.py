from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable
from Objects.Components.checklist import Checklist


class ChecklistPopup(BasePage, ICloseable):

    window_locator = '//div[@class="pop-over is-shown"]'
    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    checklist_title_locator = '//input[@id="id-checklist"]'
    add_checklist_locator = '//input[@class="primary wide confirm js-add-checklist"]'

    def __init__(self, driver, title):
        # type (WebDriver, str) -> ChecklistPopup

        BasePage.__init__(self, driver)
        self.driver = driver
        self.title = title

    def close(self):
        self.get_element(By.XPATH, self.close_locator).click()

    def complete_title(self, title):
        self.get_element(By.XPATH, self.checklist_title_locator).type(title)

    def add_checklist(self, checklist_parent):
        self.complete_title(self.title)
        self.get_element(By.XPATH, self.add_checklist_locator).click()
        return Checklist(self.driver, checklist_parent, self.title)




