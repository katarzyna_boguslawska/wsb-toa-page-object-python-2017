from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable
from Objects.Components.CardMenu.Popups.EditLabelPopup import EditLabelPopup


class LabelPopup(BasePage, ICloseable):

    window_locator = '//div[@class="pop-over is-shown"]'
    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    label_input_locator = '//input[@class="js-autofocus js-label-search"]'
    green_locator = '//span[@class="card-label mod-selectable card-label-green  js-select-label"]'
    yellow_locator = '//span[@class="card-label mod-selectable card-label-yellow  js-select-label"]'
    orange_locator = '//span[@class="card-label mod-selectable card-label-orange  js-select-label"]'
    red_locator = '//span[@class="card-label mod-selectable card-label-red  js-select-label"]'
    purple_locator = '//span[@class="card-label mod-selectable card-label-purple  js-select-label"]'
    blue_locator = '//span[@class="card-label mod-selectable card-label-blue  js-select-label"]'
    create_new_label_locator = '//a[@class="quiet-button full js-add-label"]'
    color_blind_mode_locator = '//a[@class="quiet-button full js-toggle-color-blind-mode"]'
    selected_label_tick_locator = '//span[@class="icon-sm icon-check card-label-selectable-icon light"]'
    edit_label_locator = '//a[@class="card-label-edit-button icon-sm icon-edit js-edit-label"]'

    def __init__(self, driver, label):
        # type (WebDriver, str) -> LabelPopup

        BasePage.__init__(self, driver)
        self.driver = driver
        self.label = label

    def close(self):
        self.get_element(By.XPATH, self.close_locator).click()

    def search_and_select(self, search_what):
        self.get_element(By.XPATH, self.label_input_locator).type(search_what).click_enter()
        self.close()

    def get_locator_to_color(self, color):
        requested_color = color.lower()
        if requested_color == 'green':
            return self.green_locator
        elif requested_color == 'yellow':
            return self.yellow_locator
        elif requested_color == 'orange':
            return self.orange_locator
        elif requested_color == 'red':
            return self.red_locator
        elif requested_color == 'purple':
            return self.purple_locator
        elif requested_color == 'blue':
            return self.blue_locator

    def label(self, what_color):
        color_locator = self.get_locator_to_color(what_color)
        self.get_element(By.XPATH, color_locator).click()
        self.close()

    def deselect_label(self, what_color):
        color_locator = self.get_locator_to_color(what_color)
        label = self.get_element(By.XPATH, color_locator)
        self.get_element(By.XPATH, self.selected_label_tick_locator, label).click()

    def edit_label(self, name, what_color):
        color_locator = self.get_locator_to_color(what_color)
        if color_locator:
            color_area = (By.XPATH, color_locator + '/parent::li')
            self.get_element(By.XPATH, self.edit_label_locator, color_area).click()
        else:
            self.get_element(By.XPATH, self.edit_label_locator).click()
        return EditLabelPopup(self.driver, name, what_color)


