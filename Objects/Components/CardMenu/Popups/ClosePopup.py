from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Extensions.confirmation_page import ConfirmationPage
from Objects.Interfaces.i_closeable import ICloseable


class ClosePopup(BasePage, ICloseable):

    close_component_button_locator = '//input[@class="js-confirm full negate" and @type="submit"]'
    close_popup_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    back_arrow_locator = '//a[@class="pop-over-header-back-btn icon-sm icon-back is-shown"]'

    def __init__(self, driver):
        # type (WebDriver) -> ClosePopup
        BasePage.__init__(self, driver)
        self.driver = driver

    def close_card(self):
        # type () -> ConfirmationPage
        self.get_element(By.XPATH, self.close_component_button_locator).click()

    def close(self):
        # type () -> None
        self.get_element(By.XPATH, self.close_popup_locator).click()

    def go_back(self, share_and_more_menu):
        self.get_element(By.XPATH, self.back_arrow_locator).click()
        return share_and_more_menu
