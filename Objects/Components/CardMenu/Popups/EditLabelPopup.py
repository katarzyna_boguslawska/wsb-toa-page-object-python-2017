from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable
from Objects.Interfaces.i_deletable import IDeletable


class EditLabelPopup(BasePage, ICloseable, IDeletable):

    window_locator = '//div[@class="pop-over is-shown"]'
    close_locator = './/a[@class=class="pop-over-header-close-btn icon-sm icon-close"]'

    label_input_locator = '//input[@id="labelName"]'

    green_locator = './/span[@class="card-label mod-edit-label mod-clickable card-label-green palette-color js-palette-color"]'
    yellow_locator = './/span[@class="card-label mod-edit-label mod-clickable card-label-yellow palette-color js-palette-color"]'
    orange_locator = './/span[@class="card-label mod-edit-label mod-clickable card-label-orange palette-color js-palette-color"]'
    red_locator = './/span[@class="class="card-label mod-edit-label mod-clickable card-label-red palette-color js-palette-color"]'
    purple_locator = './/span[@class="card-label mod-edit-label mod-clickable card-label-purple palette-color js-palette-color"]'
    blue_locator = './/span[@class="card-label mod-edit-label mod-clickable card-label-blue palette-color js-palette-color"]'
    turquoise_locator = './/span[@class="card-label mod-edit-label mod-clickable card-label-sky palette-color js-palette-color"]'
    pistachio_locator = './/span[@class="card-label mod-edit-label mod-clickable card-label-lime palette-color js-palette-color"]'
    pink_locator = './/span[@class="card-label mod-edit-label mod-clickable card-label-pink palette-color js-palette-color"]'
    gray_locator = './/span[@class="card-label mod-edit-label mod-clickable card-label-back palette-color js-palette-color"]'
    no_color_locator = './/span[@class="card-label mod-edit-label mod-clickable card-label-null palette-color js-palette-color"]'

    save_label_button_locator = './/input[@class="primary wide js-submit" and @type="submit"]'
    delete_label_button_locator = './/input[@type="submit" and @value="Delete"]'

    back_arrow_locator = './/a[@class="pop-over-header-back-btn icon-sm icon-back is-shown"]'

    def __init__(self, driver, name, color):
        # type (WebDriver, str) -> EditLabelPopup

        BasePage.__init__(self, driver)
        self.driver = driver
        self.name = name
        self.color = color

    def close(self):
        self.get_element(By.XPATH, self.close_locator, By.XPATH, self.window_locator).click()

    def search_and_select(self, search_what):
        self.get_element(By.XPATH, self.label_input_locator, By.XPATH, self.window_locator).type(search_what).click_enter()

    def get_locator_to_color(self, color):
        color = color.lower()
        if color == 'green':
            return self.green_locator
        elif color == 'yellow':
            return self.yellow_locator
        elif color == 'orange':
            return self.orange_locator
        elif color == 'red':
            return self.red_locator
        elif color == 'purple':
            return self.purple_locator
        elif color == 'blue':
            return self.blue_locator
        elif color == 'turquoise':
            return self.turquoise_locator
        elif color == 'pistachio':
            return self.pistachio_locator
        elif color == 'pink':
            return self.pink_locator
        elif color == 'gray':
            return self.gray_locator
        elif color == 'no_color':
            return self.no_color_locator
        else:
            raise NotImplementedError('No such color for label')

    def name_label(self, name):
        self.get_element(By.XPATH, self.label_input_locator, By.XPATH, self.window_locator).type(name)

    def choose_color(self, what_color):
        color_locator = self.get_locator_to_color(what_color)
        self.get_element(By.XPATH, color_locator, By.XPATH, self.window_locator).click()

    def save(self):
        self.get_element(By.XPATH, self.save_label_button_locator, By.XPATH, self.window_locator).click()

    def delete(self):
        self.get_element(By.XPATH, self.delete_label_button_locator, By.XPATH, self.window_locator)

    def add_label(self, name, color):
        self.name_label(name)
        self.choose_color(color)
        self.save()


