from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable
from Objects.Components.CardMenu.Popups.EditLabelPopup import EditLabelPopup


class DueDatePopup(BasePage, ICloseable):

    window_locator = '//div[@class="pop-over is-shown"]'
    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    date_input_locator = '//input[@class="datepicker-select-input js-dpicker-date-input js-autofocus"]'
    add_due_date_locator = '//input[@class="primary wide confirm" and @tabindex="103"]'
    remove_due_date_locator = '//button[@class="negate remove-date js-remove-date"]'

    def __init__(self, driver, date):
        # type (WebDriver, str) -> LabelPopup
        BasePage.__init__(self, driver)
        self.driver = driver
        self.date = date

    def close(self):
        self.get_element(By.XPATH, self.close_locator).click()

    def add_due_date(self):
        date_input = self.get_element(By.XPATH, self.date_input_locator)
        date_input.focus_and_type(self.date).click_enter()

    def remove_due_date(self):
        self.get_element(By.XPATH, self.remove_due_date_locator).click()
