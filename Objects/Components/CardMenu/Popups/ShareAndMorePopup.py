from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable
from Objects.json_export_page import JsonExportedPage
from Objects.Components.CardMenu.Popups.ClosePopup import ClosePopup


class ShareAndMorePopup(BasePage, ICloseable):

    window_locator = '//div[@class="pop-over is-shown"]'
    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    json_export_locator = '//a[@class="js-export-json"]'
    delete_locator = '//a[@class="js-delete"]'

    def __init__(self, driver, link):
        # type (WebDriver, str) -> ShareAndMorePopup
        BasePage.__init__(self, driver)
        self.driver = driver
        self.link = link

    def close(self):
        self.get_element(By.XPATH, self.close_locator).click()

    def export_to_json(self, card_menu):
        self.get_element(By.XPATH, self.json_export_locator).click()
        return JsonExportedPage(self.driver, card_menu)

    def get_delete_card_menu(self):
        self.get_element(By.XPATH, self.delete_locator).click()
        return ClosePopup(self.driver)

    def delete_card(self):
        self.get_delete_card_menu().close_card()
