from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable
from Objects.Components.attachment import Attachment


class AttachmentPopup(BasePage, ICloseable):

    window_locator = '//div[@class="pop-over is-shown"]'
    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    link_input_locator = '//input[@id="addLink"]'
    add_link_locator = '//input[@class="js-add-attachment-url"]'

    def __init__(self, driver, link):
        # type (WebDriver, str) -> AttachmentPopup
        BasePage.__init__(self, driver)
        self.driver = driver
        self.link = link

    def close(self):
        self.get_element(By.XPATH, self.close_locator).click()

    def add_attachment(self, parent):
        self.get_element(By.XPATH, self.link_input_locator).type(self.link)
        self.get_element(By.XPATH, self.add_link_locator).click()
        return Attachment(self.driver, parent, self.link)
