from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable


class MemberPopup(BasePage, ICloseable):

    window_locator = '//div[@class="pop-over is-shown"]'
    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    member_input_locator = '//input[@class="js-search-mem js-autofocus"]'

    def __init__(self, driver, member):
        # type (WebDriver, str) -> MemberPopup

        BasePage.__init__(self, driver)
        self.driver = driver
        self.member = member

    def close(self):
        self.get_element(By.XPATH, self.close_locator).click()

    def search_and_select(self, member):
        self.get_element(By.XPATH, self.member_input_locator).type(member).click_enter()
        self.close()

    def choose_member_from_list(self):
        self.get_element(By.XPATH, '//a[@class="name js-select-member " and starts-with(@title, "' + self.member + '")]').click()

    def add_member(self):
        self.choose_member_from_list()
        self.close()




