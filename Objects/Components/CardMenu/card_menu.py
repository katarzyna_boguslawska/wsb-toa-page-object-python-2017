from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Components.comment import Comment
from Objects.Interfaces.i_closeable import ICloseable
from Objects.Components.CardMenu.Popups.LabelPopup import LabelPopup
from Objects.Components.CardMenu.Popups.MemberPopup import MemberPopup
from Objects.Components.CardMenu.Popups.ChecklistPopup import ChecklistPopup
from Objects.Components.CardMenu.Popups.DueDatePopup import DueDatePopup
from Objects.Components.CardMenu.Popups.AttachmentPopup import AttachmentPopup
from Objects.Components.CardMenu.Popups.ShareAndMorePopup import ShareAndMorePopup


class CardMenu(BasePage, ICloseable):
    window_locator = '//div[@class="window"]'
    members_locator = '//a[@class="button-link js-change-card-members"]'
    labels_locator = '//a[@class="button-link js-edit-labels"]'
    checklist_locator = '//a[@class="button-link js-add-checklist-menu"]'
    due_date_locator = '//a[@class="button-link js-add-due-date"]'
    attachment_locator = '//a[@class="button-link js-attach"]'
    archive_locator = '//a[@class="button-link js-archive-card"]'
    share_and_more_locator = '//a[@class="quiet-button js-more-menu"]'
    close_locator = '//a[@class="icon-lg icon-close dialog-close-button js-close-window"]'
    close_locator_on_cover = '//a[@class="icon-lg icon-close dialog-close-button js-close-window dialog-close-button-light"]'

    # checklist submenu locators
    generic_checklist_locator = '//div[@class="checklist"]'

    # comment submenu locators
    comment_xpath = './/textarea[@class="comment-box-input js-new-comment-input"]'
    emoji_path = './/a[@class="comment-box-options-item js-comment-add-emoji"]'
    submit_comment_xpath = './/input[@class="primary confirm mod-no-top-bottom-margin js-add-comment"]'
    generic_comment_xpath = './/div[@class="phenom mod-comment-type"]'

    def __init__(self, driver, card):
        # type (WebDriver, str) -> CardMenu

        BasePage.__init__(self, driver)
        self.driver = driver
        self.card = card
        self.checklists = []
        self.activities = []
        self.attachments = []
        self.window = '//div[@class="window"]'

    def close(self):
        # type () -> bool
        try:
            self.get_element(By.XPATH, self.close_locator).click()
        except TimeoutException:
            self.get_element(By.XPATH, self.close_locator_on_cover).click()

    def open_labelling_menu(self, label):
        self.get_element(By.XPATH, self.labels_locator).click()
        return LabelPopup(self.driver, label)

    def open_members_menu(self, member):
        self.get_element(By.XPATH, self.members_locator).click()
        return MemberPopup(self.driver, member)

    def open_checklist_menu(self, title):
        self.get_element(By.XPATH, self.checklist_locator).click()
        return ChecklistPopup(self.driver, title)

    def open_due_date_menu(self, date):
        self.get_element(By.XPATH, self.due_date_locator).click()
        return DueDatePopup(self.driver, date)

    def open_attachment_menu(self, link):
        self.get_element(By.XPATH, self.attachment_locator).click()
        return AttachmentPopup(self.driver, link)

    def open_share_and_more_menu(self):
        self.get_element(By.XPATH, self.share_and_more_locator).click()
        return ShareAndMorePopup(self.driver, self)

    def archive_card(self):
        # type () -> bool
        self.get_element(By.XPATH, self.archive_locator).click()
        self.close()

    def add_due_date(self, date):
        # type (str) -> bool
        due_date_popup = self.open_due_date_menu(date)
        due_date_popup.add_due_date()
        self.close()

    def add_attachment_from_link(self, link):
        # type (str) -> bool
        attachment = self.create_attachment_from_link(link)
        self.attachments.append(attachment)
        self.close()

    def create_attachment_from_link(self, link):
        # type (str) -> Attachment
        attachment_popup = self.open_attachment_menu(link)
        parent = self.get_attachment_container(link)
        return attachment_popup.add_attachment(parent)

    @staticmethod
    def get_attachment_container(link):
        # type (str) -> str
        filename = link.rsplit('/', 1)[-1]
        return '//div[@class="u-clearfix js-attachment-list ui-sortable" and .//a[@class="attachment-thumbnail-preview js-open-viewer attachment-thumbnail-preview-is-cover" and contains(@style, "' + filename + '")]]'

    def export_to_json(self):
        # type () -> JsonExportedPage
        share_and_more_popup = self.open_share_and_more_menu()
        return share_and_more_popup.export_to_json(self)

    def create_checklist(self, name):
        # type (str) -> Checklist
        checklist_popup = self.open_checklist_menu(name)
        parent = '//div[@class="checklist" and .//h3[.="' + name + '"]]'
        return checklist_popup.add_checklist(parent)

    def add_checklist(self, name):
        # type (str) -> None
        trello_checklist = self.create_checklist(name)
        self.checklists.append(trello_checklist)

    def count_checklists(self):
        # type () -> int
        return len(self.get_elements(By.XPATH, self.generic_checklist_locator))

    def find_checklist_by_title(self, checklist_title):
        # type (str) -> Checklist
        for checklist in self.checklists:
            if checklist.name == checklist_title:
                return checklist

    def label_card(self, what_color):
        # type (str) -> None
        label_popup = self.open_labelling_menu(what_color)
        label_popup.search_and_select(what_color)
        self.close()

    def add_member(self, who):
        # type (str) -> None
        member_popup = self.open_members_menu(who)
        member_popup.add_member()
        self.close()

    def is_comment(self, comment_content):
        # type (str) -> bool
        comment = self.find_comment_by_text(comment_content)
        if comment.get_comment_content():
            return True
        else:
            return False

    def comment(self, comment_content):
        # type (str) -> None
        comment = self.add_comment(comment_content)
        self.activities.append(comment)
        self.close()

    def comment_with_emoji(self, comment_content, emoji):
        # type (str, str) -> None
        comment = self.add_comment_with_emoji(comment_content, emoji)
        self.activities.append(comment)
        self.close()

    @staticmethod
    def get_comment_container(comment_content):
        # type (str) -> str
        return './/div[@class="phenom mod-comment-type" and .//p[starts-with(text(), "' + comment_content + '")]]'

    def add_comment_with_emoji(self, comment_content, emoji):
        # type (str, str) -> Comment
        self.get_element(By.XPATH, self.comment_xpath, By.XPATH, self.window).focus_and_type(comment_content)
        self.get_element(By.XPATH, './/a[@class="comment-box-options-item js-comment-add-emoji"]', By.XPATH, self.window).click()
        self.get_element(By.XPATH, '//a[@class="name js-select-emoji" and @href="#" and @title="' + emoji + '"]').click()
        self.get_element(By.XPATH, self.submit_comment_xpath, By.XPATH, self.window).click()
        comment_container = self.get_comment_container(comment_content)
        return Comment(self.driver, comment_container, comment_content)

    def add_comment(self, comment_content):
        # type (str, str) -> Comment
        self.get_element(By.XPATH, self.comment_xpath, By.XPATH, self.window).focus_and_type(comment_content)
        self.get_element(By.XPATH, self.submit_comment_xpath, By.XPATH, self.window).click()
        comment_container = self.get_comment_container(comment_content)
        return Comment(self.driver, comment_container, comment_content)

    def find_comment_by_text(self, comment_content):
        # type (str) -> Comment
        for comment in self.activities:
            if comment.content == comment_content:
                return comment

    def delete_comment(self, comment_content):
        # type (str) -> None
        comment = self.add_comment(comment_content)
        self.activities.append(comment)
        comment.delete()
        self.activities.remove(comment)
        self.close()
