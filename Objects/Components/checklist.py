import time

from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_deletable import IDeletable


class Checklist(BasePage, IDeletable):

    delete_checklist_locator = './/a[@class="hide-on-edit quiet js-confirm-delete"]'
    close_locator = '//a[@class="icon-lg icon-close dialog-close-button js-close-window"]'
    delete_locator = '//input[@value="Delete Checklist"]'

    generic_item_xpath = './/div[@class="checklist-item"]'
    progress_bar_xpath = './/span[@class="checklist-progress-percentage js-checklist-progress-percent"]'
    generic_header_xpath = './/h3'

    # item submenu locators
    item_content_xpath = './/textarea[@class="checklist-new-item-text js-new-checklist-item-input"]'
    add_item_xpath = './/input[@class="primary confirm mod-submit-edit js-add-checklist-item"]'
    quit_adding_items_xpath = './/a[@class="icon-lg icon-close dark-hover cancel js-cancel-checklist-item"]'

    def __init__(self, driver, parent, name):
        # type (WebDriver, str, str) -> Checklist
        BasePage.__init__(self, driver)
        self.parent = parent
        self.driver = driver
        self.name = name
        self.items = []

    def count_items(self):
        # type () -> int
        all_items = self.get_elements(By.XPATH, self.generic_item_xpath, By.XPATH, self.parent)
        return len(all_items)

    def add_item(self, content):
        # type () -> None
        self.items.append(self.create_item(content))

    def create_item(self, content):
        # type (str) -> Checklist
        self.get_element(By.XPATH, self.item_content_xpath, By.XPATH, self.parent).focus_and_type(content)
        self.get_element(By.XPATH, self.add_item_xpath, By.XPATH, self.parent).click()
        self.get_element(By.XPATH, self.quit_adding_items_xpath, By.XPATH, self.parent).click()
        container = self.get_item_container(content)
        return ChecklistItem(self.driver, container, content)

    @staticmethod
    def get_item_container(content):
        # type (str) -> PageElement
        return './/div[@class="checklist-item" and .//p[text()="' + content + '"]]'

    def get_item_by_content(self, content):
        # type (str) -> ChecklistItem
        for item in self.items:
            if item.text == content:
                return item

    def delete(self):
        # type () -> None
        self.get_element(By.XPATH, self.delete_checklist_locator, By.XPATH, self.parent).click()
        self.get_element(By.XPATH, self.delete_locator).click()
        self.get_element(By.XPATH, self.close_locator).click()

    def check_if_list_has_title(self, title):
        # type (str) -> bool
        if self.get_element(By.XPATH, self.generic_header_xpath, By.XPATH, self.parent).text == title:
            return True
        else:
            return False

    def check_progress_percentage(self, number_of_completed_items):
        # type (str) -> bool
        time.sleep(0.5)
        all_items = float(len(self.items))
        percentage = ((float(number_of_completed_items) / all_items) * 100)
        rounded = int(percentage)
        progress_bar = self.get_element(By.XPATH, self.progress_bar_xpath, By.XPATH, self.parent)
        percentage_of_completed_items = int(progress_bar.text[:-1])
        return percentage_of_completed_items == rounded

    def complete_task(self, task_name):
        # type () -> None
        item = self.get_item_by_content(task_name)
        item.mark_item_as_done()


class ChecklistItem(BasePage):

    done_checkbox = './/span[@class="icon-sm icon-check checklist-item-checkbox-check"]'

    def __init__(self, driver, parent, text):
        # type (WebDriver, str, str) -> ChecklistItem
        BasePage.__init__(self, driver)
        self.parent = parent
        self.driver = driver
        self.text = text

    def mark_item_as_done(self):
        # type () -> None
        self.get_element(By.XPATH, self.done_checkbox, By.XPATH, self.parent).click()
