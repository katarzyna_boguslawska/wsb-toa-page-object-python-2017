from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Components.card import Card
from Objects.json_export_page import JsonExportedPage


class List(BasePage):
    all_lists_xpath = '//*[@id="board"]/div'
    all_lists_locator = '//*[@id="board"]/div'
    archive_list_locator = './/a[@class="js-close-list"]'
    list_parent_nodes_locator = '//div[@class="js-list list-wrapper"]'

    more_menu_xpath = './div/div/div[@class="list-header-extras"]/a'
    generic_card_xpath = './/a[@class="list-card js-member-droppable ui-droppable"]'
    add_card_xpath = './div/a[@class="open-card-composer js-open-card-composer"]'
    card_text_xpath = './/textarea[@class="list-card-composer-textarea js-card-title"]'
    add_xpath = './/input[@class="primary confirm mod-compact js-add-card"]'
    quit_adding_xpath = './/a[@class="icon-lg icon-close dark-hover js-cancel"]'

    def __init__(self, driver, parent, title):
        # type (WebDriver, str, str) -> List
        BasePage.__init__(self, driver)
        self.parent = parent
        self.driver = driver
        self.title = title
        self.cards = []

    def create_card(self, text_on_card):
        # type (str) -> Card
        self.get_element(By.XPATH, self.add_card_xpath, By.XPATH, self.parent).click()
        self.get_element(By.XPATH, self.card_text_xpath, By.XPATH, self.parent).type(text_on_card)
        self.get_element(By.XPATH, self.add_xpath, By.XPATH, self.parent).click()
        self.get_element(By.XPATH, self.quit_adding_xpath, By.XPATH, self.parent).click()
        return Card(self.driver, text_on_card)

    def add_card(self, text_on_card):
        # type (str) -> None
        card = self.create_card(text_on_card)
        self.cards.append(card)

    def archive(self):
        # type () -> None
        self.get_element(By.XPATH, self.more_menu_xpath, By.XPATH, self.parent).click()
        self.get_element(By.XPATH, self.archive_list_locator).click()

    def count_cards(self):
        # type () -> int
        all_cards_on_list = self.get_elements(By.XPATH, self.generic_card_xpath, By.XPATH, self.parent)
        cards = len(all_cards_on_list) + 1
        return cards

    def find_card_by_text(self, text_on_card):
        # type (str) -> Card
        for card in self.cards:
            if card.text == text_on_card:
                return card

    def add_due_date_on_card_with_text(self, text_on_card, date):
        # type (str, str) -> None
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        menu.add_due_date(date)

    def check_if_due_date_was_added_to_card_with_text(self, text_on_card):
        # type (str) -> bool
        card_to_work_with = self.find_card_by_text(text_on_card)
        return card_to_work_with.check_if_due_date_was_added()

    def archive_card_with_text(self, text_on_card):
        # type (str) -> None
        card_to_work_with = self.find_card_by_text(text_on_card)
        self.cards.remove(card_to_work_with)
        menu = card_to_work_with.show_card_menu()
        menu.archive_card()

    def check_if_card_with_text_was_archived(self, text_on_card):
        # type (str) -> bool
        return self.is_absent(By.XPATH, './/a[.="' + text_on_card + '"]', By.XPATH, self.parent)

    def add_attachment_from_link_to_card_with_text(self, text_on_card, link):
        # type (str, str) -> None
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        menu.add_attachment_from_link(link)

    def check_if_attachment_was_added_to_the_card_with_text(self, text_on_card):
        # type (str) -> bool
        card_to_work_with = self.find_card_by_text(text_on_card)
        return card_to_work_with.check_if_attachment_was_added()

    def export_to_json_card_with_text(self, text_on_card):
        # type (str) -> JsonExportedPage
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        menu.export_to_json()
        return JsonExportedPage(self.driver, menu)

    def add_checklist_to_card_with_text(self, text_on_card, checklist_title):
        # type (str, str) -> Checklist
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        return menu.create_checklist(checklist_title)

    def drag_card_with_text_in_place_of_card_with_text(self, src_card_text, dst_list, dst_card_text):
        # type (str, str, str) -> None
        card_to_drag = self.find_card_by_text(src_card_text)
        self.cards.remove(card_to_drag)
        destination = dst_list.find_card_by_text(dst_card_text).stable_parent
        card_to_drag.drag_and_drop_card(self.get_element(By.XPATH, destination))

    def check_if_card_with_text_was_dropped_in_place_of_card_with_text(self, src_card_text, dst_list):
        # type (str, str) -> bool
        try:
            dragged_card = self.find_card_by_text(src_card_text)
        except StaleElementReferenceException:
            dragged_card = None
        dropped_card = self.get_element(By.XPATH, './/*[text()="' + src_card_text + '"]', By.XPATH, dst_list.parent)
        if dropped_card and not dragged_card:
            return True
        else:
            return False

    def label_card_with_text(self, text_on_card, color):
        # type (str, str) -> None
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        menu.label_card(color)

    def check_if_card_with_text_was_labelled_with_color(self, text_on_card, color):
        # type (str, str) -> bool
        card_to_work_with = self.find_card_by_text(text_on_card)
        return card_to_work_with.check_if_has_label_of_color(color)

    def assign_member_to_card_with_text(self, text_on_card, member):
        # type (str, str) -> None
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        menu.add_member(member)

    def check_if_member_was_assigned_to_card_with_text(self, text_on_card, member):
        # type (str, str) -> bool
        card_to_work_with = self.find_card_by_text(text_on_card)
        return card_to_work_with.check_if_member_was_added(member)

    def comment_card_with_text(self, text_on_card, comment_content):
        # type (str, str) -> None
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        menu.comment(comment_content)

    def comment_card_with_text_with_emoji(self, text_on_card, comment_content, emoji):
        # type (str, str, str) -> None
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        menu.comment_with_emoji(comment_content, emoji)

    def delete_comment_from_card_with_text(self, text_on_card, comment_content):
        # type (str, str) -> None
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        menu.delete_comment(comment_content)

    def check_if_card_with_text_has_comment(self, text_on_card, comment_content):
        # type (str, str) -> bool
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        is_added = menu.is_comment(comment_content)
        menu.close()
        return is_added

    def check_if_card_with_text_has_comment_with_emoji(self, text_on_card, comment_content, emoji):
        # type (str, str, str) -> bool
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        comment = menu.find_comment_by_text(comment_content)
        is_added = comment.check_if_has_emoji(emoji)
        menu.close()
        return is_added

    def check_if_comment_was_removed_from_card_with_text(self, text_on_card, comment_content):
        # type (str, str) -> bool
        card_to_work_with = self.find_card_by_text(text_on_card)
        menu = card_to_work_with.show_card_menu()
        comment = menu.find_comment_by_text(comment_content)
        is_added = False
        if comment:
            is_added = comment.is_comment(comment_content)
        menu.close()
        return not is_added
