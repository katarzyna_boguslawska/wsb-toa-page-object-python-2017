from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Components.BoardMenu.Submenus.Background.background_submenu import BackgroundSubmenu
from Objects.Components.BoardMenu.Submenus.More.more_submenu import MoreSubmenu
from Objects.Interfaces.i_closeable import ICloseable


class BoardMenu(BasePage, ICloseable):

    more_locator = '//a[@class="board-menu-navigation-item-link js-open-more"]'
    stickers_locator = '//a[@class="board-menu-navigation-item-link js-open-stickers"]'
    power_ups_locator = '//a[@class="board-menu-navigation-item-link js-open-power-ups"]'
    filter_cards_locator = '//a[@class="board-menu-navigation-item-link js-open-card-filter"]'
    change_background_locator = '//a[@class ="board-menu-navigation-item-link js-change-background"]'
    archived_items_locator = '//a[@class="board-menu-navigation-item-link js-open-archive"]'
    close_board_locator = '//a[@class="board-menu-navigation-item-link js-close-board"]'
    confirm_board_closing_locator = '//input[@class="js-confirm full negate"]'
    permanently_delete_locator = '//a[@class="quiet js-delete"]'
    confirm_board_deleting_locator = '//input[@class="js-confirm full negate"]'
    photo_choices_locator = '//div[@class="board-backgrounds-section-tile board-backgrounds-photos-tile js-bg-photos"]'
    close_the_board_menu_locator = '//a[@class="board-menu-header-close-button icon-lg icon-close js-hide-sidebar"]'
    back_arrow_menu_button = '//a[@class="board-menu-header-back-button icon-lg icon-back js-pop-widget-view"]'

    def __init__(self, driver):
        # type (WebDriver) -> BoardMenu
        BasePage.__init__(self, driver)
        self.driver = driver

    def get_locator_for_submenu(self, submenu):
        # type (str) -> str
        submenu = submenu.lower()
        if submenu == 'more':
            return self.more_locator
        elif submenu == 'background':
            return self.change_background_locator
        elif submenu == 'power_ups':
            return self.power_ups_locator
        elif submenu == 'stickers':
            return self.stickers_locator
        elif submenu == 'filter':
            return self.filter_cards_locator
        else:
            raise NotImplementedError('No such submenu!')

    def return_submenu(self, which_submenu):
        # type (str) -> str
        submenu = which_submenu.lower()
        if submenu == 'more':
            return MoreSubmenu(self.driver)
        elif submenu == 'background':
            return BackgroundSubmenu(self.driver)
        elif submenu == 'power_ups':
            raise NotImplementedError('Did not implement this submenu!')
        elif submenu == 'stickers':
            raise NotImplementedError('Did not implement this submenu!')
        elif submenu == 'filter':
            raise NotImplementedError('Did not implement this submenu!')
        else:
            raise NotImplementedError('No such submenu!')

    def show_submenu(self, which_submenu):
        # type () -> PageElement
        submenu_locator = self.get_locator_for_submenu(which_submenu)
        self.is_clickable(By.XPATH, submenu_locator)
        self.get_element(By.XPATH, submenu_locator).click()
        return self.return_submenu(which_submenu)

    def delete_board(self):
        # type () -> None
        more_submenu = self.show_submenu('more')
        close_popup = more_submenu.delete_board()
        confirmation_page = close_popup.close_component()
        confirmation_page.confirm_closing()

    def change_background_to_color(self, which_color):
        # type (str) -> None
        background_submenu = self.show_submenu('background')
        background_submenu.change_background_to_color(which_color)

    def change_background_to_random_photo(self):
        # type () -> None
        background_submenu = self.show_submenu('background')
        background_submenu.change_background_to_random_photo()

    def close(self):
        # type () -> None
        self.get_element(By.XPATH, self.close_the_board_menu_locator).click()

    def go_back_to_menu(self):
        # type () -> None
        self.get_element(By.XPATH, self.back_arrow_menu_button).click()
        self.is_clickable(By.XPATH, self.photo_choices_locator)
        self.get_element(By.XPATH, self.back_arrow_menu_button).click()
        self.is_clickable(By.XPATH, self.close_the_board_menu_locator)
