from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Extensions.close_popup import ClosePopup
from Objects.Extensions.generic_board_menu_actions import GenericBoardMenuAction
from Objects.Interfaces.i_closeable import ICloseable


class MoreSubmenu(BasePage, ICloseable):

    close_board_locator = '//a[@class="board-menu-navigation-item-link js-close-board"]'

    def __init__(self, driver):
        # type (WebDriver) -> MoreSubmenu
        BasePage.__init__(self, driver)
        self.board_actions_manager = GenericBoardMenuAction(self.driver)
        self.driver = driver

    def delete_board(self):
        self.get_element(By.XPATH, self.close_board_locator).click()
        return ClosePopup(self.driver)

    def close(self):
        # type () -> None
        self.board_actions_manager.close()

    def go_to_main_menu(self):
        # type () -> None
        self.board_actions_manager.go_to_main_menu()
