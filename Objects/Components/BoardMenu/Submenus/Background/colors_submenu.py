from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Extensions.generic_board_menu_actions import GenericBoardMenuAction
from Objects.Interfaces.i_closeable import ICloseable


class ColorsSubmenu(BasePage, ICloseable):

    blue_locator = '//div[@class="image" and @style="background-color: rgb(0, 121, 191);"]'
    orange_locator = '//div[@class="image" and @style="background-color: rgb(210, 144, 52);"]'
    green_locator = '//div[@class="image" and @style="background-color: rgb(81, 152, 57);"]'
    red_locator = '//div[@class="image" and @style="background-color: rgb(176, 50, 70);"]'
    purple_locator = '//div[@class="image" and @style="background-color: rgb(137, 96, 158);"]'
    pink_locator = '//div[@class="image" and @style="background-color: rgb(205, 90, 145);"]'
    pistachio_locator = '//div[@class="image" and @style="background-color: rgb(75, 191, 107);"]'
    turquoise_locator = '//div[@class="image" and @style="background-color: rgb(0, 174, 204);"]'
    gray_locator = '//div[@class="image" and @style="background-color: rgb(131, 140, 145);"]'

    def __init__(self, driver):
        # type (WebDriver) -> ColorsSubmenu
        BasePage.__init__(self, driver)
        self.board_actions_manager = GenericBoardMenuAction(self.driver)
        self.driver = driver

    def get_locator_to_color(self, color):
        # type (str) -> str
        color = color.lower()
        if color == 'blue':
            return self.blue_locator
        elif color == 'orange':
            return self.orange_locator
        elif color == 'green':
            return self.green_locator
        elif color == 'red':
            return self.red_locator
        elif color == 'purple':
            return self.purple_locator
        elif color == 'pink':
            return self.pink_locator
        elif color == 'pistachio':
            return self.pistachio_locator
        elif color == 'turquoise':
            return self.turquoise_locator
        elif color == 'gray':
            return self.gray_locator
        else:
            raise NotImplementedError("No such color")

    def change_to_color(self, color):
        # type (str) -> None
        color_locator = self.get_locator_to_color(color)
        self.get_element(By.XPATH, color_locator).click()

    def close(self):
        # type () -> None
        self.board_actions_manager.close()

    def go_to_main_menu(self):
        # type () -> None
        self.board_actions_manager.go_to_main_menu()
