from random import randint

from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Extensions.generic_board_menu_actions import GenericBoardMenuAction
from Objects.Interfaces.i_closeable import ICloseable


class PhotosSubmenu(BasePage, ICloseable):

    generic_backgorund_photo_locator = '//div[@class="board-background-select"]'

    def __init__(self, driver):
        # type (WebDriver) -> PhotosSubmenu
        BasePage.__init__(self, driver)
        self.board_actions_manager = GenericBoardMenuAction(self.driver)
        self.driver = driver

    def list_all_photos(self):
        # type () -> PageElement[]
        return self.get_elements(By.XPATH, self.generic_backgorund_photo_locator)

    def get_random_photo(self):
        # type () -> PageElement
        index = randint(0, 11)
        return self.list_all_photos()[index]

    def change_to_random_photo(self):
        # type () -> None
        self.get_random_photo().click()

    def close(self):
        # type () -> None
        self.board_actions_manager.close()

    def go_to_main_menu(self):
        # type () -> None
        self.board_actions_manager.go_to_main_menu()
