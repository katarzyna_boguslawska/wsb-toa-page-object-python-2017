from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Components.BoardMenu.Submenus.Background.colors_submenu import ColorsSubmenu
from Objects.Components.BoardMenu.Submenus.Background.photos_submenu import PhotosSubmenu
from Objects.Extensions.generic_board_menu_actions import GenericBoardMenuAction
from Objects.Interfaces.i_closeable import ICloseable


class BackgroundSubmenu(BasePage, ICloseable):

    color_choices_locator = '//div[@class="board-backgrounds-section-tile board-backgrounds-colors-tile js-bg-colors"]/div[@class="image"]'
    photo_choices_locator = '//div[@class="board-backgrounds-section-tile board-backgrounds-photos-tile js-bg-photos"]'

    def __init__(self, driver):
        # type (WebDriver) -> BackgroundSubmenu

        BasePage.__init__(self, driver)
        self.board_actions_manager = GenericBoardMenuAction(self.driver)
        self.driver = driver

    def choose_colors_submenu(self):
        # type () -> None
        self.get_element(By.XPATH, self.color_choices_locator).click()
        return ColorsSubmenu(self.driver)

    def choose_photo_submenu(self):
        # type () -> None
        self.get_element(By.XPATH, self.photo_choices_locator).click()
        return PhotosSubmenu(self.driver)

    def change_background_to_color(self, color):
        # type (str) -> None
        color_submenu = self.choose_colors_submenu()
        color_submenu.change_to_color(color)

    def change_background_to_random_photo(self):
        # type () -> None
        photo_submenu = self.choose_photo_submenu()
        photo_submenu.change_to_random_photo()

    def close(self):
        # type () -> None
        self.board_actions_manager.close()

    def go_to_main_menu(self):
        # type () -> None
        self.board_actions_manager.go_to_main_menu()
