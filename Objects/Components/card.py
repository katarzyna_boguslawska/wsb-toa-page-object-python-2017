from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Components.CardMenu.card_menu import CardMenu


class Card(BasePage):

    card_menu_xpath = './/a[@class="list-card js-member-droppable ui-droppable"]'
    future_due_date_xpath = './/div[@class="badge is-due-future"]'
    image_xpath = './/div[@class="list-card-cover js-card-cover"]'
    green_xpath = './/span[@class="card-label card-label-green mod-card-front"]'
    yellow_xpath = './/span[@class="card-label card-label-yellow mod-card-front"]'
    orange_xpath = './/span[@class="card-label card-label-orange mod-card-front"]'
    red_xpath = './/span[@class="card-label card-label-red mod-card-front"]'
    purple_xpath = './/span[@class="card-label card-label-purple mod-card-front"]'
    blue_xpath = './/span[@class="card-label card-label-blue mod-card-front"]'
    menu = None

    def __init__(self, driver, text_on_card):
        # type (WebDriver, str) -> Card
        BasePage.__init__(self, driver)
        self.driver = driver
        self.text = text_on_card
        self.newly_created_card_parent = '//a[.="' + self.text + '"]/../..'
        self.stable_parent = '//a[@class="list-card js-member-droppable ui-droppable" and .//span[text()="' + self.text + '"]]'

    def show_card_menu(self):
        # type () -> CardMenu
        self.get_element(By.XPATH, self.stable_parent).click()
        if not self.menu:
            self.menu = CardMenu(self.driver, self.stable_parent)
        return self.menu

    def drag_and_drop_card(self, destination):
        # type (PageElement) -> None
        self.get_element(By.XPATH, self.stable_parent).drag_to(destination)

    def check_if_due_date_was_added(self):
        # type () -> bool
        if self.get_element(By.XPATH, self.future_due_date_xpath, By.XPATH, self.stable_parent):
            return True
        else:
            return False

    def check_if_attachment_was_added(self):
        # type () -> bool
        if self.get_element(By.XPATH, self.image_xpath, By.XPATH, self.stable_parent):
            return True
        else:
            return False

    def check_if_has_label_of_color(self, color):
        # type (str) -> bool
        color = color.lower()
        if color == 'green':
            try:
                if self.get_element(By.XPATH, self.green_xpath, By.XPATH, self.stable_parent):
                    return True
            except TimeoutException:
                return False
        elif color == 'yellow':
            try:
                if self.get_element(By.XPATH, self.yellow_xpath, By.XPATH, self.stable_parent):
                    return True
            except TimeoutException:
                return False
        elif color == 'orange':
            try:
                if self.get_element(By.XPATH, self.orange_xpath, By.XPATH, self.stable_parent):
                    return True
            except TimeoutException:
                return False
        elif color == 'red':
            try:
                if self.get_element(By.XPATH, self.red_xpath, By.XPATH, self.stable_parent):
                    return True
            except TimeoutException:
                return False
        elif color == 'purple':
            try:
                if self.get_element(By.XPATH, self.purple_xpath, By.XPATH, self.stable_parent):
                    return True
            except TimeoutException:
                return False
        elif color == 'blue':
            try:
                if self.get_element(By.XPATH, self.blue_xpath, By.XPATH, self.stable_parent):
                    return True
            except TimeoutException:
                return False
        else:
            return False

    def check_if_member_was_added(self, who):
        # type (str) -> bool
        try:
            if self.get_element(By.XPATH, './/span[@class="member-initials" and starts-with(@title,  "' + who + '")]', By.XPATH, self.stable_parent):
                return True
        except TimeoutException:
            return False
