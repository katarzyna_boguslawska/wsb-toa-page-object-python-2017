from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable


class UserMenu(BasePage, ICloseable):

    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    profile_locator = '//a[@class="js-profile"]'
    cards_locator = '//a[@class="js-cards"]'
    settings_locator = '//a[@class="js-account"]'
    help_locator = '//a[@class="js-help"]'
    shortcuts_locator = '//a[@class="js-shortcuts"]'
    change_language_locator = '//a[@class="js-change-locale"]'
    logout_locator = '//a[@class="js-logout"]'

    def __init__(self, driver):
        # type (WebDriver) -> UserMenu
        BasePage.__init__(self, driver)
        self.driver = driver

    def close(self):
        # type () -> None
        self.get_element(By.XPATH, self.close_locator).click()

    def log_out(self):
        # type () -> None
        self.get_element(By.XPATH, self.logout_locator).click()
