from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable
from Objects.TeamManagementPages.business_team_creator import BusinessTeamCreator
from Objects.TeamManagementPages.personal_team_creator import PersonalTeamCreator


class CreationMenu(BasePage, ICloseable):

    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    create_board_locator = '//a[@class="js-new-board"]'
    create_personal_team_locator = '//a[@class="js-new-org"]'
    create_business_team_locator = '//a[@class="js-new-bc-org"]'

    def __init__(self, driver, generic_actions_manager):
        # type (WebDriver) -> CreationMenu
        BasePage.__init__(self, driver)
        self.driver = driver
        self.generic_actions_manager = generic_actions_manager

    def open_board_creation_page(self):
        # type () -> None
        self.get_element(By.XPATH, self.create_board_locator).click()

    def open_personal_team_creation_page(self, name, description=None):
        # type () -> None
        self.get_element(By.XPATH, self.create_personal_team_locator).click()
        return PersonalTeamCreator(self.driver, name, self.generic_actions_manager, description=None)

    def open_business_team_creation_page(self, name, description=None):
        # type () -> None
        self.get_element(By.XPATH, self.create_business_team_locator).click()
        return BusinessTeamCreator(self.driver, name, self.generic_actions_manager, description=None)

    def close(self):
        # type () -> None
        self.get_element(By.XPATH, self.close_locator).click()
