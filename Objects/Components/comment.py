from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_deletable import IDeletable


class Comment(BasePage, IDeletable):

    edit_locator = './/a[@class="js-edit-action"]'
    delete_locator = './/a[@class="js-confirm-delete-action"]'
    confirm_deleting_locator = './/input[@class="js-confirm full negate"]'

    comment_content_xpath = './/div[@class="current-comment js-friendly-links js-open-card"]/p'

    def __init__(self, driver, parent, content):
        # type (WebDriver, str, str) -> Comment

        BasePage.__init__(self, driver)
        self.content = content
        self.driver = driver
        self.initial_parent = parent
        self.reliable_parent = '//div[@class="phenom mod-comment-type" and .//*[text()="' + self.content + '"]]'

    def check_if_has_emoji(self, emoji):
        # type (str) -> bool
        try:
            if self.get_element(By.XPATH, './/img[@title="' + emoji + '" and @class="emoji"]', By.XPATH, self.initial_parent):
                return True
        except TimeoutException:
            return False

    def get_comment_content(self):
        # type () -> str
        return self.get_element(By.XPATH, self.comment_content_xpath, By.XPATH, self.initial_parent).text

    def delete(self):
        # type () -> None
        self.is_visible(By.XPATH, '//p[starts-with(text(), "' + self.content + '")]')
        self.get_element(By.XPATH, self.delete_locator, By.XPATH, self.reliable_parent).click()
        self.get_element(By.XPATH, self.confirm_deleting_locator).click()
