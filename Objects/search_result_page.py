from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage


class SearchResult(BasePage):

    result_view_locator = '//div[@class="search-results-view"]'
    generic_result_locator = '//div[@class="search-result-card"]'
    boards_button_locator = '//a[@class="header-btn header-boards js-boards-menu"]'

    def __init__(self, driver, search_phrase, generic_actions_manager):
        BasePage.__init__(self, driver)
        self.driver = driver
        self.search_phrase = search_phrase
        self.generic_actions_manager = generic_actions_manager

    def is_result_found(self):
        if self.get_element(By.XPATH, '//span[@title="' + self.search_phrase + '"]'):
            return True
        else:
            return False

    def log_out(self):
        self.generic_actions_manager.log_out()

    def go_to_main_dashboard(self):
        self.generic_actions_manager.go_to_main_dashboard()

    def search(self, what):
        return self.generic_actions_manager.search(what)

    def go_to_board(self, which_board):
        self.generic_actions_manager.go_to_board(which_board)
