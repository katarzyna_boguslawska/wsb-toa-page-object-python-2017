from selenium.webdriver.common.by import By
import Utils.settings
from Objects.Abstractions.base_page import BasePage
from Objects.Extensions.generic_logged_in_actions import GenericLoggedInAction
from Objects.group_dashboard_page import GroupDashboard
from Objects.personal_dashboard_page import PersonalDashboard
from Utils import settings


class LoggedUserDashboard(BasePage):

    create_new_board_locator = '//a[@class="board-tile mod-add"]'
    board_title_input_locator = '//input[@id="boardNewTitle"]'
    create_board_button = '//input[@class="primary wide js-submit" and @type="submit" and @value="Create"]'
    board_relative_xpath = './/a[@class="board-tile mod-add" and @href="#"]'

    def __init__(self, driver):
        BasePage.__init__(self, driver)
        self.generic_actions_manager = GenericLoggedInAction(driver)
        self.url = Utils.settings.VALID_LOGIN_URL
        self.driver = driver
        self.boards = []

    def is_create_board_element_present(self):
        return self.is_present(By.XPATH, self.create_new_board_locator)

    def locate_group_area(self, group_name):
        return self.get_element(By.XPATH, '//h3[@class="boards-page-board-section-header-name" and contains(text(),"' + group_name + '")]/../..')

    def create_new_group_board(self, group_name, board_name):
        group_div = self.locate_group_area(group_name)
        self.get_element(By.XPATH, self.board_relative_xpath, group_div).click()
        self.get_element(By.XPATH, self.board_title_input_locator).type(board_name)
        self.get_element(By.XPATH, self.create_board_button).click()
        return GroupDashboard(self.driver, self.generic_actions_manager)

    def add_group_board(self, group_name, board_name):
        board = self.create_new_group_board(group_name, board_name)
        self.boards.append(board)

    def create_new_personal_board(self, board_name):
        personal_div = self.locate_group_area(settings.PERSONAL_BOARD)
        self.get_element(By.XPATH, self.board_relative_xpath, personal_div).click()
        self.get_element(By.XPATH, self.board_title_input_locator).type(board_name)
        self.get_element(By.XPATH, self.create_board_button).click()
        return PersonalDashboard(self.driver, self.generic_actions_manager)

    def add_personal_board(self, board_name):
        board = self.create_new_personal_board(board_name)
        self.boards.append(board)

    def open_plus_menu(self):
        return self.generic_actions_manager.open_plus_menu()

    def open_user_menu(self):
        return self.generic_actions_manager.open_user_menu()

    def open_boards_menu(self):
        self.generic_actions_manager.open_boards_menu()

    def log_out(self):
        self.generic_actions_manager.log_out()

    def go_to_main_dashboard(self):
        self.generic_actions_manager.go_to_main_dashboard()

    def search(self, what):
        return self.generic_actions_manager.search(what)

    def go_to_board(self, which_board):
        self.generic_actions_manager.go_to_board(which_board)
