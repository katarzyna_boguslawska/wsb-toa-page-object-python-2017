from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.TeamManagementPages.Subprofiles.Popups.MemberPopup import MemberPopup
from Utils import settings
import time


class MembersProfile(BasePage):

    add_by_name_locator = '//a[@class="primary button-link autowrap" and .//span[text()="Add by Name or Email"]]'
    generic_member = '.member-list-item-detail'

    def __init__(self, driver, name):
        BasePage.__init__(self, driver)
        self.team_name = name

    def open_adding_member_popup(self, who):
        self.get_element(By.XPATH, self.add_by_name_locator).click()
        return MemberPopup(self.driver, who)

    def add_member(self, who):
        member_popup = self.open_adding_member_popup(who)
        member_popup.add_member(who)

    def count_members(self):
        return len(self.get_elements(By.CSS_SELECTOR, self.generic_member))

    def check_if_members_number_increased(self, from_what):
        how_many_attempts = int(settings.GENERAL_TIMEOUT / 0.2)
        for attempt in range(0, how_many_attempts):
            new_members_number = self.count_members()
            if new_members_number > from_what:
                return new_members_number
            else:
                attempt += 1
                time.sleep(0.2)
