from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable


class VisibilityPopup(BasePage, ICloseable):

    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    private_locator = '//a[@class="highlight-icon js-select-members"]'
    public_locator = '//a[@class="highlight-icon js-select-public"]'

    def __init__(self, driver):
        BasePage.__init__(self, driver)

    def get_locator_to_visibility(self, visibility):
        visibility = visibility.lower()
        if visibility == 'private':
            return self.private_locator
        elif visibility == 'public':
            return self.public_locator
        else:
            raise NotImplementedError("No such visibility")

    def close(self):
        self.get_element(By.XPATH, self.close_locator)

    def change_visibility(self, to_what_visibility):
        visibility_locator = self.get_locator_to_visibility(to_what_visibility)
        self.get_element(By.XPATH, visibility_locator).click()

