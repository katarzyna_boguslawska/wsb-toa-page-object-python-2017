from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable


class MemberPopup(BasePage, ICloseable):

    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    name_input = '//input[@class="js-search-input js-autofocus" and @type="text"]'
    send_invitation_locator = '//input[@class="wide primary js-send-email-invite" and @type="submit"]'

    def __init__(self, driver, who):
        BasePage.__init__(self, driver)
        self.member = who

    def close(self):
        self.get_element(By.XPATH, self.close_locator)

    def add_member(self, name):
        member_name_input = self.get_element(By.XPATH, self.name_input)
        member_name_input.type(name)
        try:
            self.get_element(By.CSS_SELECTOR, self.send_invitation_locator).click()
        except TimeoutException:
            member_name_input.click_enter()





