from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable
from Objects.Interfaces.i_deletable import IDeletable


class DeletionPopup(BasePage, ICloseable, IDeletable):

    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    confirm_deleting_locator = '//input[@class="js-confirm full negate" and @type="submit"]'

    def __init__(self, driver):
        BasePage.__init__(self, driver)

    def close(self):
        self.get_element(By.XPATH, self.close_locator)

    def delete(self):
        self.get_element(By.XPATH, self.confirm_deleting_locator).click()

