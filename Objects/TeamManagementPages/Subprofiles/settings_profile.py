from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_deletable import IDeletable
from Objects.TeamManagementPages.Subprofiles.Popups.VisibilityPopup import VisibilityPopup
from Objects.TeamManagementPages.Subprofiles.Popups.DeletionPopup import DeletionPopup
from selenium.common.exceptions import TimeoutException


class SettingsProfile(BasePage, IDeletable):

    private_visibility_locator = '//span[text()="Private"]'
    public_visibility_locator = '//span[text()="Public"]'
    change_visibility_locator = '//a[@class="button-link u-text-align-center" and .//span[text()="Change"]]'
    deleting_team_locator = '//a[@class="quiet-button" and .//span[text()="Delete this team?"]]'

    def __init__(self, driver, name):
        BasePage.__init__(self, driver)
        self.team_name = name

    def get_current_visibility(self):
        try:
            self.get_element(By.XPATH, self.private_visibility_locator)
            return 'private'
        except TimeoutException:
            return 'public'

    def open_visibility_menu(self):
        self.get_element(By.XPATH, self.change_visibility_locator).click()
        return VisibilityPopup(self.driver)

    def open_deleting_menu(self):
        self.get_element(By.XPATH, self.deleting_team_locator).click()
        return DeletionPopup(self.driver)

    def delete(self):
        deletion_popup = self.open_deleting_menu()
        deletion_popup.delete()

    def change_visibility_to(self, what_visibility):
        visibility_popup = self.open_visibility_menu()
        visibility_popup.change_visibility(what_visibility)
