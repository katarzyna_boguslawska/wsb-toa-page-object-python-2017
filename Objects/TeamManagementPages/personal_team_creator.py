from selenium.webdriver.common.by import By

from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_closeable import ICloseable
from Objects.Interfaces.i_team_creatable import ITeamCreatable
from Objects.TeamManagementPages.personal_team_profile import PersonalTeamProfile


class PersonalTeamCreator(BasePage, ITeamCreatable, ICloseable):
    name_input_locator = '//input[@id="org-display-name"]'
    description_locator = '//textarea[@id="org-desc"]'
    create_button_locator = '//input[@class="primary wide js-save" and @type="submit"]'
    close_locator = '//a[@class="pop-over-header-close-btn icon-sm icon-close"]'
    back_locator = '//a[@class="pop-over-header-back-btn icon-sm icon-back is-shown"]'

    def __init__(self, driver, name, generic_actions_manager, description=None):
        # type (WebDriver, str, str) -> PersonalTeamCreator
        BasePage.__init__(self, driver)
        self.driver = driver
        self.team_name = name
        self.generic_actions_manager = generic_actions_manager
        self.team_description = description

    def close(self):
        # type () -> None
        self.get_element(By.XPATH, self.close_locator).click()

    def go_back_to_creation_menu(self):
        # type () -> None
        self.get_element(By.XPATH, self.back_locator).click()

    def complete_name(self, name):
        # type (str) -> None
        self.get_element(By.XPATH, self.name_input_locator).focus_and_type(name)

    def complete_description(self, description):
        # type (str) -> None
        self.get_element(By.XPATH, self.description_locator).focus_and_type(description)

    def click_create(self, name):
        # type () -> None
        self.get_element(By.XPATH, self.create_button_locator).click()
        return PersonalTeamProfile(self.driver, name, self.generic_actions_manager)

    def create_team(self, name, description=None):
        # type (str, str) -> None
        self.complete_name(name)
        if description:
            self.complete_description(description)
        return self.click_create(name)
