from selenium.webdriver.common.by import By
from Objects.Abstractions.base_page import BasePage
from Objects.Interfaces.i_deletable import IDeletable
from Objects.Interfaces.i_profilable import IProfilable
from Objects.TeamManagementPages.Subprofiles.members_profile import MembersProfile
from Objects.TeamManagementPages.Subprofiles.settings_profile import SettingsProfile


class PersonalTeamProfile(BasePage, IProfilable, IDeletable):

    edit_profile_locator = '//a[@class="button-link tabbed-pane-header-details-edit js-edit-profile"]'
    boards_locator = '//a[@class="tabbed-pane-nav-item-button js-org-profile"]'
    members_locator = '//a[@class="tabbed-pane-nav-item-button js-org-members"]'
    settings_locator = '//a[@class="tabbed-pane-nav-item-button js-org-account"]'
    business_class_locator = '//a[@class="tabbed-pane-nav-item-button dark-hover js-billing"]'
    settings_profile = None
    members_profile = None

    def __init__(self, driver, name, generic_actions_manager):
        BasePage.__init__(self, driver)
        self.team_name = name
        self.team_name_locator = '//h1[text()="' + self.team_name + '"]'
        self.generic_actions_manager = generic_actions_manager

    def edit_team_profile(self):
        self.get_element(By.XPATH, self.edit_profile_locator).click()

    def go_to_boards(self):
        self.get_element(By.XPATH, self.boards_locator).click()

    def go_to_members(self):
        self.get_element(By.XPATH, self.members_locator).click()
        if not self.members_profile:
            self.members_profile = MembersProfile(self.driver, self.team_name)
        return self.members_profile

    def go_to_settings(self):
        self.get_element(By.XPATH, self.settings_locator).click()
        if not self.settings_profile:
            self.settings_profile = SettingsProfile(self.driver, self.team_name)
        return self.settings_profile

    def go_to_business_class(self):
        self.get_element(By.XPATH, self.business_class_locator).click()

    def change_visibility(self, to_what_visibility):
        self.go_to_settings().change_visibility_to(to_what_visibility)
        return self.settings_profile

    def add_member(self, who):
        self.go_to_members().add_member(who)
        return self.members_profile

    def delete(self):
        self.go_to_settings().delete()

    def is_team_created(self):
        is_visible = self.is_visible(By.XPATH, self.team_name_locator)
        name = self.team_name.lower().replace(' ', '')
        is_in_url = name in self.driver.current_url
        return is_visible and is_in_url

    def log_out(self):
        self.generic_actions_manager.log_out()

    def go_to_main_dashboard(self):
        self.generic_actions_manager.go_to_main_dashboard()

    def search(self, what):
        return self.generic_actions_manager.search(what)

    def go_to_board(self, which_board):
        self.generic_actions_manager.go_to_board(which_board)

