from abc import ABCMeta, abstractmethod


class IProfilable:
    __metaclass__ = ABCMeta

    @abstractmethod
    def edit_team_profile(self):
        raise NotImplementedError("Class derived from IProfilable must override edit_team_profile()")

    def go_to_boards(self):
        raise NotImplementedError("Class derived from IProfilable must override go_to_boards()")

    def go_to_members(self):
        raise NotImplementedError("Class derived from IProfilable must override go_to_members()")

    def go_to_settings(self):
        raise NotImplementedError("Class derived from IProfilable must override go_to_setings()")

    def go_to_business_class(self):
        raise NotImplementedError("Class derived from IProfilable must override go_to_business_class()")

    def is_team_created(self):
        raise NotImplementedError("Class derived from IProfilable must override is_team_created()")

