from abc import ABCMeta, abstractmethod


class ICloseable:
    __metaclass__ = ABCMeta

    @abstractmethod
    def close(self):
        raise NotImplementedError("Class derived from ICloseable must override close()")
