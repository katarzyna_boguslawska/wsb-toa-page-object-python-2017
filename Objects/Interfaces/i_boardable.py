from abc import ABCMeta, abstractmethod


class IBoardable:
    __metaclass__ = ABCMeta

    @abstractmethod
    def is_board_visible(self, title):
        raise NotImplementedError("Class derived from IBoardable must override is_board_visible(title)")

    @abstractmethod
    def create_list(self, list_title):
        raise NotImplementedError("Class derived from IBoardable must override create_list(list_title)")

    @abstractmethod
    def add_list(self, list_title):
        raise NotImplementedError("Class derived from IBoardable must override add_list(list_title)")

    @abstractmethod
    def find_list_by_title(self, title):
        raise NotImplementedError("Class derived from IBoardable must override find_list_by_title(title)")

    @abstractmethod
    def archive_list_with_title(self, list_title):
        raise NotImplementedError("Class derived from IBoardable must override archive_list_with_title(list_title)")

    @abstractmethod
    def count_lists(self):
        raise NotImplementedError("Class derived from IBoardable must override count_lists()")

    @abstractmethod
    def show_menu(self):
        raise NotImplementedError("Class derived from IBoardable must override show_menu()")

    @abstractmethod
    def delete_board(self):
        raise NotImplementedError("Class derived from IBoardable must override delete_board()")

    @abstractmethod
    def get_background_color(self):
        raise NotImplementedError("Class derived from IBoardable must override get_background_color()")

    @abstractmethod
    def check_if_background_is_photo(self):
        raise NotImplementedError("Class derived from IBoardable must override get_background_photo()")

    @abstractmethod
    def log_out(self):
        raise NotImplementedError("Class derived from IBoardable must override log_out()")

    @abstractmethod
    def rename(self, to_what):
        raise NotImplementedError("Class derived from IBoardable must override rename(to_what)")
