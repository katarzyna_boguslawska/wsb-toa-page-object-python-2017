from abc import ABCMeta, abstractmethod


class ITeamCreatable():
    __metaclass__ = ABCMeta

    @abstractmethod
    def complete_name(self, name):
        raise NotImplementedError("Class derived from TeamCreatable must override complete_name(name)")

    def complete_description(self, description):
        raise NotImplementedError("Class derived from TeamCreatable must override complete_description(description)")

    def click_create(self, name):
        raise NotImplementedError("Class derived from TeamCreatable must override click_create()")

    def create_team(self, name, description=None):
        raise NotImplementedError("Class derived from TeamCreatable must override create_team(name)")

