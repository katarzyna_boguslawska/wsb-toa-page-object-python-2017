from abc import ABCMeta, abstractmethod


class IDeletable:
    __metaclass__ = ABCMeta

    @abstractmethod
    def delete(self):
        raise NotImplementedError("Class derived from IDeletable must override delete()")
