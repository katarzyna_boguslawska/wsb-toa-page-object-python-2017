#!/usr/bin python
from Utils import run_all_tests
from ProcessTools import log_parser
from ProcessTools import mail_parser
from ProcessTools import db_utils
from ProcessTools import settings as process_settings
import os


def check_and_run_tests():
    is_test_start = mail_parser.read_emails(process_settings.LOGIN, process_settings.PASSWORD, process_settings.SERVER, process_settings.SUBJECT)
    if is_test_start:
        os.chdir(process_settings.TEST_REPOSITORY)
        run_all_tests.get_all_tests()
        the_file = log_parser.LogParser.get_logfile_name(process_settings.LOG_DIR)
        full_path = os.path.join(process_settings.LOG_DIR, the_file)
        was_successful = log_parser.LogParser.is_overall_result_success(full_path)
        complete_json = log_parser.LogParser.parse_logs()
        db_utils.insert_test_results(complete_json)
        if was_successful:
            mail_parser.send_mail(process_settings.SUCCESS_TEMPLATE,
                                  process_settings.LOGIN,
                                  process_settings.LOGIN,
                                  process_settings.PASSWORD,
                                  process_settings.SERVER,
                                  process_settings.PORT,
                                  'Automated Trello test results - PASSED')
        else:
            mail_parser.send_mail(process_settings.FAILURE_TEMPLATE,
                                  process_settings.LOGIN,
                                  process_settings.LOGIN,
                                  process_settings.PASSWORD,
                                  process_settings.SERVER,
                                  process_settings.PORT,
                                  'Automated Trello test results - FAILED')
    else:
        print 'Doing nothing'


if __name__ == '__main__':
    check_and_run_tests()
